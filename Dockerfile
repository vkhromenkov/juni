FROM cypress/base:16.14.2-slim AS builder
WORKDIR /opt
COPY . .
RUN npm ci &&\
    $(npm bin)/cypress cache path &&\
    $(npm bin)/cypress cache list &&\
    $(npm bin)/cypress verify

FROM cypress/browsers:node16.14.2-slim-chrome103-ff102
LABEL image="cypress"
WORKDIR /opt
COPY --from=builder /root/.cache /root/.cache
COPY --from=builder /opt/ .
