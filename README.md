# Juni - Unidata CE UI testing framework, based on Cypress

## Configuration
It is necessary to create environment file ```cypress.env.json``` in root directory with:
```json
{
  "uiUrl": "http://localhost:8080/unidata-frontend",
  "apiUrl": "http://localhost:8080/unidata-backend/api/",
  "apiVer": "v2",
  "login": "...",
  "password": "..."
}
```

## Installation
1) Install Node.js (version 18+)
2) Install dependencies
```bash
npm install
```

## Tests
- Open Cypress tests in Chrome browser without rerun on code change
```bash
npm run open 
```
- Run all tests on given browser (chrome, firefox, edge)
```bash
npm run test -- --browser given_browser
```
- Run specific spec
```bash
npx cypress run --spec "path/to/spec/file.spec.js"
```
