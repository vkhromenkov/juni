import { defineConfig } from 'cypress';
import { configureAllureAdapterPlugins } from '@mmisty/cypress-allure-adapter/plugins';

export default defineConfig({
  defaultCommandTimeout: 60000,
  responseTimeout: 60000,
  viewportWidth: 1920,
  viewportHeight: 1080,
  video: false,
  reporter: 'junit',
  reporterOptions: {
    mochaFile: 'junit-results/test-result.[hash].xml',
    testsuitesTitle: 'Juni Tests',
    jenkinsMode: true,
  },
  e2e: {
    setupNodeEvents(on, config) {
      const baseUrl: string = config.env.uiUrl || null;
      if (baseUrl) {
        config.baseUrl = baseUrl;
      }
      configureAllureAdapterPlugins(on, config);
      return config;
    },
  },
});
