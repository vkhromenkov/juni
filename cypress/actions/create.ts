import { faker } from '@faker-js/faker';
import * as joda from '@js-joda/core';
import * as format from './format';

export function person(firstName: string = faker.person.firstName(), lastName: string = faker.person.lastName(), validFrom?: joda.LocalDateTime, validTo?: joda.LocalDateTime,
  externalId: string | null = faker.string.uuid(), etalonId?: string) {
  cy.fixture('data/person').then((request) => {
    request.dataRecord.externalId.externalId = externalId;
    if (typeof etalonId !== 'undefined') {
      request.dataRecord.etalonId = etalonId;
    }
    request.dataRecord.simpleAttributes[0].value = lastName;
    request.dataRecord.simpleAttributes[1].value = firstName;
    if (typeof validFrom !== 'undefined') {
      request.dataRecord.validFrom = format.dateForRestApi(validFrom);
    }
    if (typeof validTo !== 'undefined') {
      request.dataRecord.validTo = format.dateForRestApi(validTo);
    }
    cy.createRecord(request);
  });
}

export function job(name: string = faker.string.alpha(7), blockSize: number = faker.number.int(100000)) {
  cy.fixture('job').then((request) => {
    request.definition.displayName = name;
    request.definition.parameters.blockSize = blockSize;
    cy.createJob(request);
  });
}

export function personWithAddress() {
  cy.fixture('data/personWithAddress').then((request) => {
    request.dataRecord.externalId.externalId = faker.string.uuid();
    request.dataRecord.simpleAttributes[0].value = faker.person.lastName();
    request.dataRecord.simpleAttributes[1].value = faker.person.firstName();
    request.relationContains.toUpdate[0].record.externalId.externalId = faker.string.uuid();
    request.relationContains.toUpdate[0].record.simpleAttributes[0].value = faker.location.zipCode();
    request.relationContains.toUpdate[0].record.simpleAttributes[1].value = faker.location.street();
    request.relationContains.toUpdate[0].record.simpleAttributes[2].value = faker.location.city();
    cy.createRecord(request);
  });
}

export function automobile(model: string = faker.vehicle.model()) {
  cy.fixture('data/automobile').then((request) => {
    request.dataRecord.externalId.externalId = faker.string.uuid();
    request.dataRecord.simpleAttributes[0].value = model;
    cy.createRecord(request);
  });
}

export function personWithCar(autoRecordId: string) {
  cy.fixture('data/personWithCar').then((request) => {
    request.dataRecord.externalId.externalId = faker.string.uuid();
    request.dataRecord.simpleAttributes[0].value = faker.person.lastName();
    request.dataRecord.simpleAttributes[1].value = faker.person.firstName();
    request.relationManyToMany.toUpdate[0].etalonIdTo = autoRecordId;
    request.relationManyToMany.toUpdate[0].simpleAttributes[0].value = faker.number.int({ min: 2000, max: 2030 });
    request.relationManyToMany.toUpdate[0].simpleAttributes[1].value = faker.number.int();
    cy.createRecord(request);
  });
}

export function organization(orgName: string = faker.company.name()) {
  cy.fixture('data/organization').then((request) => {
    request.dataRecord.externalId.externalId = faker.string.uuid();
    request.dataRecord.simpleAttributes[0].value = orgName;
    cy.createRecord(request);
  });
}

export function personWithPlaceOfWork(orgRecordId: string) {
  cy.fixture('data/personWithPlaceOfWork').then((request) => {
    request.dataRecord.externalId.externalId = faker.string.uuid();
    request.dataRecord.simpleAttributes[0].value = faker.person.lastName();
    request.dataRecord.simpleAttributes[1].value = faker.person.firstName();
    request.relationReferences.toUpdate[0].etalonIdTo = orgRecordId;
    request.relationReferences.toUpdate[0].simpleAttributes[0].value = faker.number.int();
    request.relationReferences.toUpdate[0].simpleAttributes[1].value = faker.person.jobTitle();
    cy.createRecord(request);
  });
}

export function nested(displayName: string = faker.string.alpha({ length: 10, casing: 'upper' })) {
  cy.fixture('model/nested').then((request) => {
    request.nestedEntity.name = faker.string.alpha(10);
    request.nestedEntity.displayName = displayName;
    request.nestedEntity.simpleAttributes[0].name = faker.string.alpha(10);
    request.nestedEntity.simpleAttributes[0].displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    cy.createNested(request);
  });
}

export function lookup(displayName: string = faker.string.alpha({ length: 10, casing: 'upper' })) {
  cy.fixture('model/lookup').then((request) => {
    request.lookupEntity.name = faker.string.alpha(10);
    request.lookupEntity.displayName = displayName;
    request.lookupEntity.groupName = "ROOT";
    request.lookupEntity.codeAttribute.name = faker.string.alpha(10);
    request.lookupEntity.codeAttribute.displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    cy.createLookup(request);
  });
}

export function entity(displayName: string = faker.string.alpha({ length: 10, casing: 'upper' })) {
  cy.fixture('model/entity').then((request) => {
    request.registerEntity.name = faker.string.alpha(10);
    request.registerEntity.displayName = displayName;
    request.registerEntity.groupName = "ROOT";
    request.registerEntity.simpleAttributes[0].name = faker.string.alpha(10);
    request.registerEntity.simpleAttributes[0].displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    cy.createEntity(request);
  });
}

export function matchingTable(displayName: string, name: string = faker.string.alpha(10)) {
  cy.fixture('matching/table').then((request) => {
    request.matchingTable.name = name;
    request.matchingTable.displayName = displayName;
    cy.createMatchingTable(request);
  });
}

export function matchingRule(displayName: string, name: string = faker.string.alpha(10)) {
  cy.fixture('matching/rule').then((request) => {
    request.rule.name = name;
    request.rule.displayName = displayName;
    cy.createMatchingRule(request);
  });
}

export function matchingSet(displayName: string, mathcingTable: string, rule: string, name: string = faker.string.alpha(10)) {
  cy.fixture('matching/set').then((request) => {
    request.set.name = name;
    request.set.displayName = displayName;
    request.set.matchingTable = mathcingTable;
    request.set.mappings[0].ruleName = rule;
    request.set.mappings[0].mappings[0].settingsId = rule + '_ExactAlgorithm_0';
    request.set.mappings[0].mappings[1].settingsId = rule + '_ExactAlgorithm_1';
    cy.createMatchingSet(request);
  });
}

export function dqRule(displayName: string, name: string = faker.string.alpha(10)) {
  cy.fixture('dq/rule').then((request) => {
    request.rule.name = name;
    request.rule.displayName = displayName;
    request.rule.cleanseFunctionName = "LowerCase";
    request.rule.runCondition = "RUN_NEVER";
    cy.createRule(request);
  });
}

export function dqSet(displayName: string, rule: string, name: string = faker.string.alpha(10)) {
  cy.fixture('dq/set').then((request) => {
    request.set.name = name;
    request.set.displayName = displayName;
    request.set.mappings[0].ruleName = rule;
    cy.createSetOfRules(request);
  });
}
