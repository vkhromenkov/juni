import * as joda from '@js-joda/core';
import * as formatter from "../units/formatter";

export function date(value: joda.LocalDate | joda.LocalDateTime) {
    return value.format(formatter.date());
}

export function dateWithTime(value: joda.LocalDateTime) {
    return value.format(formatter.dateTime());
}

export function dateForRestApi(value: joda.LocalDateTime) {
    return value.format(formatter.restApi());
}