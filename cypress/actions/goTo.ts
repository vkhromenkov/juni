function goTo(href: string) {
    cy.get('button[data-qaid=main]').should('be.visible').click();
    cy.get('div[data-qaid=main-menu]').should('be.visible').within(() => {
        cy.get(`a.ud-main-menu-item[href='${href}']`).click();
    });
}

export function goToUsers() {
    goTo('#/users');
    cy.get('[data-qaid=usersPage]').should('be.visible');
}

export function goToMain() {
    goTo('#/dashboard');
    cy.get('[data-qaid=mainPage]').should('be.visible');
}

export function goToRoles() {
    goTo('#/roles');
    cy.get('[data-qaid=rolePage]').should('be.visible');
    cy.get('div[class^=siderListContainer] div[class^=rowContainer]').should('have.length.at.least', 1);
}

export function goToLabels() {
    goTo('#/security');
    cy.get('[data-qaid=securityLabelPage]').should('be.visible');
}

export function goToSourceSystems() {
    cy.intercept('GET', '**/meta/source-systems*').as('ss');
    goTo('#/sourcesystems');
    cy.get('[data-qaid=sourceSystemsPage]').should('be.visible');
    cy.wait('@ss');
}

export function goToEnumerations() {
    goTo('#/enumerations');
    cy.get('[data-qaid=enumerationPage]').should('be.visible');
}

export function goToUnits() {
    goTo('#/units');
    cy.get('[data-qaid=measurementPage]').should('be.visible');
}

export function goToDataModel() {
    goTo('#/metamodel');
    cy.get('[data-qaid=metaModelPage]').should('be.visible');
}
export function goToData() {
    goTo('#/search');
    cy.get('[data-qaid=searchPage]').should('be.visible');
}

export function goToImportExport() {
    goTo('#/ie');
    cy.get('[data-qaid=importExportPage]').should('be.visible');
}

export function goToJobs() {
    goTo('#/operations/list');
    cy.get('[data-qaid=jobsPage]').should('be.visible');
}

export function goToAuditLog() {
    cy.intercept('POST', '**/audit/search*').as('audit');
    goTo('#/auditlog');
    cy.get('[data-qaid=auditLogPage]').should('be.visible');
    cy.wait('@audit');
}

export function goToPipeline() {
    goTo('#/pipeline');
    cy.get('[data-qaid=pipelinePage]').should('be.visible');
}

export function goToBackendProperties() {
    goTo('#/backend_properties');
    cy.get('[data-qaid=backendPropertiesPage]').should('be.visible');
}

export function goToLibrary() {
    goTo('#/library');
    cy.get('[data-qaid=libraryPage]').should('be.visible');
}

export function goToDataQuality() {
    goTo('#/quality');
    cy.get('[data-qaid=qualityPage]').should('be.visible');
}

export function goToDuplicates() {
    goTo('#/cluster');
    cy.get('[data-qaid=duplicatesPage]').should('be.visible');
}

export function goToMatching() {
    goTo('#/matching');
    cy.get('[data-qaid=matchingPage]').should('be.visible');
}
