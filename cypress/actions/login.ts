export function login(username: string = Cypress.env('login'), password: string = Cypress.env('password')) {
    cy.intercept('GET', '**/model/entity-groups*').as('apiRequest');
    cy.get('input[autocomplete=username]').should('be.visible').type(username);
    cy.get('input[autocomplete=current-password]').should('be.visible').type(password);
    cy.contains('button', 'Log in').should('be.visible').click();
    cy.wait('@apiRequest');
    cy.get('div[data-qaid=mainPage]').should('be.visible');
}
