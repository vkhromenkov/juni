/**
 * Selects entity in search.
 * @example entityInSearch('Person')
 */
export function entityInSearch(entityName: string) {
    cy.get('div[class^=entityList] div[data-component-id=Select] div[class^=inputValue]').click();
    cy.get('div[data-qaid=popover_entitytree]').should('be.visible').within(() => {
        cy.get('div[class^=row]').contains(entityName).click();
    });
    cy.waitForNetworkIdle(1000);
}
