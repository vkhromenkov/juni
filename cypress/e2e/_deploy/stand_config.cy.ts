describe('Stand configuration', () => {

  it('Import model', () => {
    cy.importSourceSystems('test_model/source-systems.xml');
    cy.importUnits('test_model/measure.xml');
    cy.importEnumerations('test_model/enumerations.xml');
    cy.importDataModel('test_model/model.xml', true);
    cy.importDQModel('test_model/data-quality.xml');
    cy.importMatchingModel('test_model/matching.xml');
  });

  it('Update user passwords', () => {
    cy.setPassword(Cypress.env('login'), Cypress.env('password'));
  });

});