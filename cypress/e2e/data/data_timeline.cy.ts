import * as check from "../../actions/check";
import * as format from "../../actions/format";
import * as create from "../../actions/create";
import * as open from "../../actions/open";
import { draftSwitcher, messageDialog, fieldValue, modalWindow } from "../../units/components";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import * as joda from '@js-joda/core';
import RecordPage from '../../po/RecordPage';

describe('Data records: timeline', () => {

  const currentDate = joda.LocalDate.now();
  const startDateTime = currentDate.minusMonths(3).atStartOfDay();
  const middleDateTimeEnd = currentDate.atStartOfDay().minus(joda.Duration.ofMillis(1));
  const middleDateTimeStart = currentDate.atStartOfDay();
  const endDateTime = currentDate.plusMonths(3).plusDays(1).atStartOfDay().minus(joda.Duration.ofMillis(1));

  beforeEach(() => {
    cy.visit('/');
    login();
  });

  it('Add time interval', () => {
    // Create new record using REST API
    create.person();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Click 'Add time interval' button
    cy.contains('button', 'Add time interval').should('be.visible').click();
    // Fill time interval dates
    modalWindow('Create new time interval').should('be.visible').within(() => {
      cy.get("input[placeholder='01/01/1900']").click().clear().type(format.dateWithTime(startDateTime));
      cy.get("input[placeholder='12/31/2500']").click().clear().type(format.dateWithTime(endDateTime) + '{enter}');
    });
    check.toast('Record is saved');
    modalWindow('Create new time interval').should('not.exist');
    // Publish draft
    new RecordPage().publish();
    cy.contains('button', 'Add time interval').should('not.exist');
    // Check resulted time intervals
    cy.get('div[class^=timelinesSection]').should('be.visible').within(() => {
      cy.get('div[class^=timelineContainer]').should('have.length', 3);
      cy.get('div[class^=timelineContainer]:nth-child(2)').invoke('attr', 'class').should('contain', 'isSelected');
      cy.get('div[class^=timelineContainer]:nth-child(2)>span:nth-child(2)').should('contain', `${format.date(startDateTime)}\u00a0-\u00a0${format.date(endDateTime)}`);
    });
  });

  it('Edit data record for time interval', () => {
    // Create first record time interval using REST API
    const lastName = faker.person.lastName();
    const firstName = faker.person.firstName();
    create.person(firstName, lastName, startDateTime, middleDateTimeEnd);
    // Create second record time interval using REST API
    cy.get('@etalonId').then(etalonId => {
      create.person(firstName, lastName, middleDateTimeStart, endDateTime, null, etalonId);
    });
    // Prepare data for updated record time interval
    const updLastName = faker.person.lastName();
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Edit attribute value
    fieldValue('Last Name').click();
    fieldValue('Last Name').find('input').clear().type(updLastName).should('have.value', updLastName);
    // Save record
    cy.contains('button', 'Save').click();
    check.toast('Record is saved');
    // Publish draft
    new RecordPage().publish();
    // Check first time interval
    cy.get('div[class^=timelinesSection]').should('be.visible').within(() => {
      cy.get('div[class^=timelineContainer]').should('have.length', 2);
      // Switch to first time interval
      cy.get('div[class^=timelineContainer]:nth-child(1)>span:nth-child(2)').
        should('have.text', `${format.date(startDateTime)}\u00a0-\u00a0${format.date(middleDateTimeEnd)}`).click();
      cy.get('div[class^=timelineContainer]:nth-child(1)').invoke('attr', 'class').should('contain', 'isSelected');
    });
    fieldValue('Last Name').find('input').should('have.value', lastName);
    fieldValue('First Name').find('input').should('have.value', firstName);
    // Check second time interval
    cy.get('div[class^=timelinesSection]').should('be.visible').within(() => {
      // Switch to second time interval
      cy.get('div[class^=timelineContainer]:nth-child(2)>span:nth-child(2)').
        should('have.text', `${format.date(middleDateTimeStart)}\u00a0-\u00a0${format.date(endDateTime)}`).click();
      cy.get('div[class^=timelineContainer]:nth-child(2)').invoke('attr', 'class').should('contain', 'isSelected');
    });
    fieldValue('Last Name').find('input').should('have.value', updLastName);
    fieldValue('First Name').find('input').should('have.value', firstName);
  });

  it('Delete time interval', () => {
    // Create first record time interval using REST API
    const lastName = faker.person.lastName();
    const firstName = faker.person.firstName();
    create.person(firstName, lastName, startDateTime, middleDateTimeEnd);
    // Create second record time interval using REST API
    cy.get('@etalonId').then(etalonId => {
      create.person(firstName, lastName, middleDateTimeStart, endDateTime, null, etalonId);
    });
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Click delete time interval button
    cy.get('div[class^=timelineContainer]:nth-child(2)>span:nth-child(1)').should('be.visible').click();
    // Confirm delete
    messageDialog().should('be.visible').find('button[data-qaid=confirm]').click();
    // Check inactive time interval is not displayed
    cy.get('div[class^=timelineContainer]:nth-child(2)').should('not.exist');
    // Publish draft
    new RecordPage().publish();
    // Check inactive time interval is not displayed
    cy.get('div[class^=timelineContainer]:nth-child(2)').should('not.exist');
    // Check deleted inactive time interval is displayed if option selected
    cy.get('div[class^=switcher] button[data-component-id=Button').click();
    cy.get('div[class^=popover]').should('be.visible').find('input[type=checkbox]').check();
    cy.get('div[class^=timelineContainer]:nth-child(2)').invoke('attr', 'class').should('contain', 'isInactive');
  });

  it('Restore time interval', () => {
    // Create first record time interval using REST API
    const lastName = faker.person.lastName();
    const firstName = faker.person.firstName();
    create.person(firstName, lastName, startDateTime, middleDateTimeEnd);
    // Create second record time interval using REST API and remove it
    cy.get('@etalonId').then(etalonId => {
      create.person(firstName, lastName, middleDateTimeStart, endDateTime, null, etalonId);
      cy.deleteRecordTimeline('person', etalonId, format.dateForRestApi(middleDateTimeStart), format.dateForRestApi(endDateTime));
    });
    // Open created record
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    cy.get('div[class^=switcher] button[data-component-id=Button').click();
    cy.get('div[class^=popover]').should('be.visible').find('input[type=checkbox]').check();
    cy.get('div[class^=timelineContainer]:nth-child(2)').should('be.visible').click();
    // Click restore time interval button
    cy.get('div[class^=timelineContainer]:nth-child(2) button').should('be.visible').click();
    // Check time interval is active
    cy.get('div[class^=timelineContainer]:nth-child(2)').invoke('attr', 'class').should('not.contain', 'isInactive');
    // Publish draft
    new RecordPage().publish();
    // Check time interval is active
    cy.get('div[class^=timelineContainer]:nth-child(2)').invoke('attr', 'class').should('not.contain', 'isInactive');
  });
});
