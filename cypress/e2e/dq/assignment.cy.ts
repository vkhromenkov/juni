import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import { pageBody, messageDialog, dropDown, draftSwitcher, panel } from "../../units/components";
import { goToDataQuality } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import DataQualityPage from '../../po/DataQualityPage';

describe('DQ advanced: assignment', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToDataQuality();
    // Switch to advanced mode
    cy.get('header button[class*=buttonHasLeftIcon]').should('be.visible').click();
    dropDown().should('be.visible').contains('Switch to advanced mode').click();
  });

  it('Create DQ rule set assignment', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const ruleName = faker.string.alpha(10);
    create.dqRule(faker.string.alpha({ length: 10, casing: 'upper' }), ruleName);
    create.dqSet(displayName, ruleName);
    // Switch to Assignments
    cy.get('div[data-component-id=TabBar]').contains('Assignments').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add assignment and save
    panel('Entities').within(() => {
      cy.contains('button', 'Add').click();
      dropDown().should('be.visible').contains('DQ Entity').should('be.visible').click();
      // Close dropdown 
      pageBody().click();
      table.rowByText('DQ Entity').should('be.visible').within(() => {
        cy.get('div[class^=cellContainer]').contains('unset').click();
        dropDown().should('be.visible').contains(displayName).click();
        cy.get('div[class^=inputValue]').click();
        dropDown().should('not.exist');
      });
    });
    // Publish draft
    new DataQualityPage().publish();
    table.rowByText('DQ Entity').should('be.visible').within(() => {
      cy.get('div[class^=cellContainer]:not([class*=pre_])').eq(1).should('have.text', displayName);
    });
  });

  it('Delete DQ rule set assignment', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const ruleName = faker.string.alpha(10);
    const setName = faker.string.alpha(10);
    create.dqRule(faker.string.alpha({ length: 10, casing: 'upper' }), ruleName);
    create.dqSet(displayName, ruleName, setName);
    cy.createAssignments(setName, 'dqEntity');
    // Switch to Set of rules
    cy.get('div[data-component-id=TabBar]').contains('Assignments').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Select created assignment
    table.rowByText('DQ Entity').trigger('mouseover').find('input[type=checkbox]').check();
    // Click delete
    cy.contains('button', 'Remove').should('be.visible').click();
    // Click Continue button in confirmation dialog
    messageDialog().should('be.visible').find('button[data-qaid=confirm]').click();
    messageDialog().should('not.exist');
    // Publish draft
    new DataQualityPage().publish();
    // Check set
    table.rowByText(displayName).should('not.exist');
  });

});