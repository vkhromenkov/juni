import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import { dropDown, textAreaByLabel, inputByLabel, draftSwitcher, fieldByLabel, optionList, panel } from "../../units/components";
import { goToDataQuality } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import DataQualityPage from '../../po/DataQualityPage';

describe('DQ advanced: rules', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToDataQuality();
    // Switch to advanced mode
    cy.get('header button[class*=buttonHasLeftIcon]').should('be.visible').click();
    dropDown().should('be.visible').contains('Switch to advanced mode').click();
  });

  it('Create DQ rule', () => {
    // Prepare data for filling
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const description = faker.lorem.sentence();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add rule
    cy.contains('button', 'Create rule').click();
    // Fill rule fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      inputByLabel('Name').type(name).should('have.value', name);
      inputByLabel('Display name').type(displayName).should('have.value', displayName);
      textAreaByLabel('Description').type(description).should('have.value', description);
      fieldByLabel('Run condition').click();
      optionList().should('be.visible').contains('Never').click();
      fieldByLabel('Run condition').find('div[class^=displayValue]').should('have.text', 'Never');
      fieldByLabel('For which source systems to apply').click();
      optionList().should('be.visible').contains('unidata').click();
      fieldByLabel('For which source systems to apply').within(() => {
        cy.get('div[class^=inputValue]').click();
        cy.get('div[class^=option]>span').should('have.text', 'unidata');
      });
      fieldByLabel('Function').click();
      dropDown().should('be.visible').contains('UpperCase').scrollIntoView().click();
      fieldByLabel('Function').find('div[class^=displayValue]').should('have.text', 'UpperCase');
      panel('Enrichment').find('button.ant-switch').click();
      cy.contains('button', 'Save').click();
      check.toast('Rule is saved');
    });
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new DataQualityPage().publish();
    // Check created rule
    table.rowByText(name).should('be.visible').within(() => {
      cy.get('div[class^=cellContainer]').eq(0).should('have.text', displayName);
      cy.get('div[class^=cellContainer]').eq(2).should('have.text', description);
      cy.get('div[class^=cellContainer]').eq(3).should('have.text', 'UpperCase');
      cy.get('div[class^=cellContainer]').eq(4).should('have.text', 'Never');
    });
  });

  it('Update DQ rule', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const updDisplayName = faker.string.alpha({ length: 10, casing: 'upper' });
    create.dqRule(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Open created rule
    table.rowByText(displayName).click();
    // Fill rule fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      inputByLabel('Display name').clear().type(updDisplayName).should('have.value', updDisplayName);
      cy.contains('button', 'Save').click();
      check.toast('Rule is saved');
    });
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new DataQualityPage().publish();
    // Check updated rule
    table.rowByText(updDisplayName).should('be.visible');
    table.rowByText(displayName).should('not.exist');
  });

  it('Delete DQ rule', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    create.dqRule(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Select created rule
    table.rowByText(displayName).trigger('mouseover').find('input[type=checkbox]').check();
    // Click remove
    cy.contains('button', 'Remove').should('be.visible').click();
    // Click Continue button in confirmation dialog
    cy.get('div[data-qaid=confirmbox]>div').should('be.visible').find('button[data-qaid=confirm]').click();
    cy.get('div[data-qaid=confirmbox]').should('not.exist');
    // Check popup notification message
    check.toast('Rule is deleted');
    // Publish draft
    new DataQualityPage().publish();
    // Check rule
    table.rowByText(displayName).should('not.exist');
  });

  it('DQ rule: Required fields', () => {
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add rule
    cy.contains('button', 'Create rule').click();
    // Fill rule fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      textAreaByLabel('Description').type('something');
      cy.contains('button', 'Save').click();
      check.toast('Validation error');
      check.fieldErrorText('Name', 'Name is required');
      check.fieldErrorText('Display name', 'Display name is required');
      check.fieldErrorText('Run condition', 'Run condition is required');
      check.fieldErrorText('For which source systems to apply', 'Source system is required');
      check.fieldErrorText('Function', 'Function name is required');
    });
  });

});