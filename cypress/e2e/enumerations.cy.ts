import * as check from "../actions/check";
import * as table from "../units/table";
import { headerButton, messageDialog, actionsButton, siderList, inputByLabel, panel } from "../units/components";
import { goToEnumerations } from "../actions/goTo";
import { login } from "../actions/login";
import { faker } from '@faker-js/faker';

describe('Enumerations', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
  });

  it('Create enumeration', () => {
    goToEnumerations();
    // Prepare data for filling
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const valueName = faker.string.alpha(5);
    const valueDisplayName = faker.string.alpha({ length: 5, casing: 'upper' });
    // Click Add enumerations
    actionsButton(1).click();
    // Fill name/display name
    inputByLabel('Name').type(name).should('have.value', name);
    inputByLabel('Display name').type(displayName).should('have.value', displayName);
    // Fill enumeration content
    panel('Enumeration content').within(() => {
      cy.contains('button', 'Add value').click();
      table.cell(1, 3).trigger('mouseover').find('div[class^=valueContainer]').click();
      table.cell(1, 3).find('input').type(valueName + '{enter}');
      table.cell(1, 4).trigger('mouseover').find('div[class^=valueContainer]').click();
      table.cell(1, 4).find('input').type(valueDisplayName + '{enter}');
    });
    // Click Save
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Enumeration is saved');
    // Check enumerations appears in list
    siderList().should('contains.text', displayName);
    // Check fields
    inputByLabel('Name').should('have.value', name);
    inputByLabel('Display name').should('have.value', displayName);
    // Fill enumeration content
    panel('Enumeration content').within(() => {
      table.cell(1, 3).should('have.text', valueName);
      table.cell(1, 4).should('have.text', valueDisplayName);
    });
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Edit enumeration', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const updDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const valueName = faker.string.alpha(5);
    const valueDName = faker.string.alpha({ length: 5, casing: 'upper' });
    cy.createEnumeration(faker.string.alpha(10), displayName, valueName, valueDName);
    goToEnumerations();
    // Open created enumerations
    siderList().contains(displayName).click();
    // Edit display name
    inputByLabel('Display name').clear().type(updDName);
    // Update enumeration content
    panel('Enumeration content').within(() => {
      // Edit first value
      table.rowByText(valueName + '1').invoke('index').then((i) => {
        table.cell(i + 1, 3).contains(valueName + '1').click();
        table.cell(i + 1, 3).find('input').clear().type(valueName + 'upd{enter}');
        table.cell(i + 1, 4).contains(valueDName + '1').click();
        table.cell(i + 1, 4).find('input').clear().type(valueDName + 'UPD{enter}');
      });
      // Delete second value
      table.rowByText(valueName + '2').trigger('mouseover').find('input[type=checkbox]').check();
      cy.contains('button', 'Remove').should('be.visible').click();
      // Add new value
      cy.contains('button', 'Add value').click();
      table.cell(2, 3).trigger('mouseover').find('div[class^=valueContainer]').click();
      table.cell(2, 3).find('input').type(valueName + 'add{enter}');
      table.cell(2, 4).trigger('mouseover').find('div[class^=valueContainer]').click();
      table.cell(2, 4).find('input').type(valueDName + 'ADD{enter}');
    });
    // Click Save
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Enumeration is saved');
    // Check updated enumeration appears in list
    siderList().should('contains.text', updDName);
    siderList().should('not.contains.text', displayName);
    // Check values
    inputByLabel('Display name').should('have.value', updDName);
    panel('Enumeration content').within(() => {
      table.rowByText(valueName + '2').should('not.exist');
      table.cell(1, 3).should('have.text', valueName + 'upd');
      table.cell(1, 4).should('have.text', valueDName + 'UPD');
      table.cell(2, 3).should('have.text', valueName + 'add');
      table.cell(2, 4).should('have.text', valueDName + 'ADD');
    });
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Delete enumeration', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    cy.createEnumeration(faker.string.alpha(10), displayName, faker.string.alpha(5), faker.string.alpha({ length: 5, casing: 'upper' }));
    goToEnumerations();
    // Open created enumerations
    siderList().contains(displayName).click();
    // Click Delete
    headerButton(2).click();
    // Confirmation box should appear
    messageDialog().should('be.visible');
    // Click Continue button
    messageDialog().find('button[data-qaid=confirm]').click();
    // Check popup notification message
    check.toast('Enumeration is deleted');
    // Check enumerations not appears in list
    siderList().should('not.contains.text', displayName);
  });

  it('Enumeration: Required fields', () => {
    goToEnumerations();
    // Click Add enumerations
    actionsButton(1).click();
    // Click Add value
    cy.contains('button', 'Add value').click();
    // Click Save
    cy.contains('button', 'Save').click();
    check.toast('Validation error');
    // Check for an error in tooltip
    check.fieldErrorText('Name', 'Name is required');
    check.fieldErrorText('Display name', 'Display name is required');
    // Check for an error block for missing values
    cy.get('div.ud-inline-message-type-error').should('be.visible').
      and('contains.text', 'Enumeration values has error').
      and('contains.text', 'Identifier is required').
      and('contains.text', 'Display name is required');
  });

  it('Enumeration: Incorrect values', () => {
    goToEnumerations();
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    // Click Add enumeration
    actionsButton(1).click();
    // Fill data
    inputByLabel('Name').type('name!');
    inputByLabel('Display name').type(displayName);
    panel('Enumeration content').within(() => {
      cy.contains('button', 'Add value').click();
      table.cell(1, 3).trigger('mouseover').find('div[class^=valueContainer]').click();
      table.cell(1, 3).find('input').type('name!{enter}');
      table.cell(1, 4).trigger('mouseover').find('div[class^=valueContainer]').click();
      table.cell(1, 4).find('input').type('dname{enter}');
    });
    // Click Save
    cy.contains('button', 'Save').click();
    check.toast('Validation error');
    // Check for an error under the name field
    check.fieldErrorText('Name', 'The value must contain latin letters, numbers, symbols "-", "_" and start with a letter. Cannot contain spaces.');
    // Check for an error block for missing values
    cy.get('div.ud-inline-message-type-error').should('be.visible').
      and('contains.text', 'Enumeration values has error').
      and('contains.text', 'The value must contain latin letters, numbers and symbols "-", "_" and start with a letter or number. Cannot contain spaces.');
  });

  it('Enumeration: Save button state', () => {
    goToEnumerations();
    // Click Add button
    actionsButton(1).click();
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
    // Fill some property
    inputByLabel('Name').type('smth');
    // Check Save button is enabled
    check.buttonIsEnabled(cy.contains('button', 'Save'));
    // Clear the value
    inputByLabel('Name').clear();
    // Check Save button is disabled again
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

});
