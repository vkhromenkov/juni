import * as check from "../actions/check";
import * as table from "../units/table";
import { inputByName, messageDialog, modalWindow } from "../units/components";
import { goToLibrary } from "../actions/goTo";
import { login } from "../actions/login";
import { faker } from '@faker-js/faker';

describe('Libraries', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToLibrary();
  });

  it('Upload and delete library', () => {
    // Prepare data
    const fileName = 'uploadLibrary.jar';
    const description = faker.lorem.sentence();
    const version = '6.1.' + faker.number.int({ min: 0, max: 9999 });
    // Add library
    cy.contains('button', 'Add library').click();
    modalWindow('Upload new library').should('be.visible').within(() => {
      // Attach file
      cy.get('input[type=file]').selectFile(`cypress/fixtures/files/${fileName}`, { force: true });
      // Fill Fields
      inputByName('version').clear().type(version).should('have.value', version);
      inputByName('description').type(description).should('have.value', description);
      // Click upload 
      cy.contains('button', 'Upload').click();
    });
    modalWindow('Upload new library').should('not.exist');
    // Check popup notification message
    check.toast('Library is uploaded');
    // Check uploaded lib
    table.rowByText(fileName).should('be.visible').within(() => {
      cy.get('div[class^=cellContainer]').eq(1).should('have.text', version);
      cy.get('div[class^=cellContainer]').eq(2).should('have.text', 'Cleanse function');
      cy.get('div[class^=cellContainer]').eq(3).should('have.text', description);
      cy.get('div[class^=cellContainer]').eq(4).should('have.text', 'admin');
    });
    // Delete uploaded lib
    table.rowByText(fileName).trigger('mouseover').find('input[type=checkbox]').check();
    cy.contains('button', 'Remove').should('be.visible').click();
    // Confirmation box should appear
    messageDialog().should('be.visible');
    // Click Continue button
    messageDialog().find('button[data-qaid=confirm]').click();
    // Check popup notification message
    check.toast('Libraries are deleted');
    // Check deleted lib isn't displayed
    table.rowByText(fileName).should('not.exist');
  });

  it('Download library', () => {
    // Prepare data
    const fileName = 'system-cleanse-functions.jar';
    const version = '1.0.0';
    // Сatch the request
    cy.intercept('GET', '**/libraries/download*').as('downloadLib');
    // Click download
    table.rowByText(fileName).trigger('mouseover');
    cy.contains('button', 'Download').should('be.visible').click();
    // Wait and check request/response
    cy.wait('@downloadLib')
      .then((interception) => {
        expect(interception.request.url).to.include(fileName);
        expect(interception.request.url).to.include(version);
        expect(interception.response!.statusCode).to.equal(200);
        expect(interception.response!.headers['content-disposition']).to.include(fileName);
        expect(interception.response!.body.byteLength).to.greaterThan(0);
      });
  });

});
