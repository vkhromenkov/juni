import * as check from "../actions/check";
import { login } from "../actions/login";
import { goToAuditLog } from "../actions/goTo";
import * as waitFor from "../actions/waitFor";
import { columnFilterButton, columnHeader, columnSortIcon } from "../units/table";
import * as joda from '@js-joda/core';
import * as formatter from "../units/formatter";

describe('Audit logs', () => {

  const noDataText = 'No data';

  beforeEach(() => {
    cy.visit('/');
    login();
    goToAuditLog();
  });

  it('Filtering', () => {
    // Filter by not existing Login
    columnFilterButton('Interface').click();
    cy.get('div#portal div[class*=popover]>div[class*=hasShadow]').should('be.visible').within(() => {
      cy.get('input[data-qaid=endpoint_filter]').should('be.visible').type('SOAP').should('have.value', 'SOAP');
      cy.contains('button', 'Apply').should('be.visible').click();
    });
    cy.get('div#portal div[class*=popover]>div[class*=hasShadow]').should('not.exist');
    // Check there is no data found by such filter
    cy.get('div[class^=emptyTableContent').should('be.visible').and('have.text', noDataText);
    // Turn off filters
    cy.get('button[data-qaid=filter_group_button]').should('be.visible').click();
    // Check there is data
    cy.get('div[class^=emptyTableContent').should('not.exist');
    cy.wait(500);
    // Turn on filters
    cy.get('button[data-qaid=filter_group_button]').should('be.visible').click();
    // Check there is no data found by such filter
    cy.get('div[class^=emptyTableContent').should('be.visible').and('have.text', noDataText);
    // Remove filters
    cy.get('button[data-qaid=trash_button]').should('be.visible').click();
    // Check there is data
    cy.get('div[class^=emptyTableContent').should('not.exist');
    // Filter by type = login
    columnFilterButton('Message type').click();
    cy.get('div#portal div[class*=popover]>div[class*=hasShadow]').should('be.visible').within(() => {
      cy.get('div[data-component-id=Select] div[class^=inputValue]').click();
    });
    cy.get('div[data-component-id=DropDown]').should('be.visible').within(() => {
      cy.get('div[class^=option]').contains('Login').click();
    });
    cy.get('div#portal div[class*=popover]>div[class*=hasShadow]').first().within(() => {
      cy.contains('button', 'Apply').should('be.visible').click();
    });
    cy.get('div#portal div[class*=popover]>div[class*=hasShadow]').should('not.exist');
    // Wait for table to load
    cy.get('div.ant-spin').should('not.exist');
    // Check there are only 'login' text in the whole column
    cy.get('div[class^=rowContainer]>div:nth-child(7)>span').each(($el) => {
      expect($el).to.contain.text('Login');
    });
  });

  it('Sorting', () => {
    // Default sorting for 'When happened' is ON
    columnSortIcon('Time').should('be.visible');
    // Check DESC sorting
    let datesDESC: joda.LocalDateTime[] = [];
    cy.get('div[class^=rowContainer]>div:nth-child(5)>span').each(($el) => {
      datesDESC.push(joda.LocalDateTime.parse($el.text(), formatter.dateTime()));
    }).wrap(datesDESC).then(() => {
      expect(datesDESC.every((val, i, arr) => !i || val.compareTo(arr[i - 1]) <= 0)).to.be.true;
    });
    // Turn off sorting
    columnHeader('Time').click();
    columnSortIcon('Time').should('not.exist');
    // Turn on ASC sorting
    columnHeader('Time').click();
    // Wait for table to load
    cy.get('div.ant-spin').should('not.exist');
    columnSortIcon('Time').should('be.visible');
    cy.wait(500);
    // Check ASC sorting
    let datesASC: joda.LocalDateTime[] = [];
    cy.get('div[class^=rowContainer]>div:nth-child(5)>span').each(($el) => {
      datesASC.push(joda.LocalDateTime.parse($el.text(), formatter.dateTime()));
    }).wrap(datesASC).then(() => {
      expect(datesASC.every((val, i, arr) => !i || val.compareTo(arr[i - 1]) >= 0)).to.be.true;
    });
  });

  it('Export', () => {
    // Click Export button and check toast message
    cy.get('button[data-qaid=export_actions]').should('be.visible').click();
    cy.get('div[data-qaid=export_page_item]').should('be.visible').click();
    check.toast('Export is running. Result will be available in Notifications');
    // Wait for notification
    waitFor.notification();
    // Open Notifications
    cy.get('button[data-qaid=userMenuButton]').click();
    cy.get('div[data-qaid=user-menu]').should('be.visible').within(() => {
      cy.contains('Notifications').click();
    });
    // Check text and link
    cy.get('div[class^=notificationArea_]').should('be.visible').within(() => {
      cy.get('div[class^=notification_]:first-child').should('be.visible').within(() => {
        cy.get('div[data-qaid=content]').should('have.text', 'Audit log export');
        cy.get('a[data-qaid=download]').should('be.visible').and('have.attr', 'href')/*.and('contain', '/core/lob/blob')*/;
      });
    });
  });

});
