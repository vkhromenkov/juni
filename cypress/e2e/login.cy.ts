import * as check from "../actions/check";
import { dropDown } from "../units/components";

describe('Login', () => {

  beforeEach(() => {
    cy.visit('/');
  });

  it('Check language selector', () => {
    cy.get('div[class^=logoContainer]>object').should('be.visible').invoke('attr', 'data').then((imageURL) => {
      expect(imageURL).to.have.string('logoplatform-dark-en.svg');
    });

    check.buttonIsDisabled(cy.get('button[class*=buttonIntentPrimary]').should('be.visible').and('have.text', 'Log in'));
    cy.get('button[class*=buttonIntentSecondary]').should('be.visible').and('have.text', 'Forgot your password?');

    cy.get('div[class^=localizationContainer]').should('be.visible').click();
    dropDown().should('be.visible').find('span[class*=iconFlagRu]').click();
    cy.get('input[autocomplete=username]').click();
    dropDown().should('not.exist');
    cy.get('div[class^=logoContainer]>object').should('be.visible').invoke('attr', 'data').then((imageURL) => {
      expect(imageURL).to.have.string('logoplatform-dark-ru.svg');
    });

    check.buttonIsDisabled(cy.get('button[class*=buttonIntentPrimary]').should('be.visible').and('have.text', 'Войти'));
    cy.get('button[class*=buttonIntentSecondary]').should('be.visible').and('have.text', 'Забыли пароль?');

  });

  it('Successfull login', () => {

    cy.get('input[autocomplete=username]').type('admin').should('have.value', 'admin');
    check.buttonIsDisabled(cy.contains('button', 'Log in').should('be.visible'));
    cy.get('input[autocomplete=current-password]').type('admin').should('have.value', 'admin');
    cy.contains('button', 'Log in').should('be.visible').as('loginButton');
    check.buttonIsEnabled(cy.get('@loginButton'));
    cy.get('@loginButton').click();

    cy.get('div[data-qaid=mainPage]').should('be.visible');
  });

  it('Login with unknown user', () => {
    cy.get('input[autocomplete=username]').type('anonymous').should('have.value', 'anonymous');
    cy.get('input[autocomplete=current-password]').type('anonymous');
    cy.contains('button', 'Log in').should('be.visible').click();
    check.toast('Authentication failed. Incorrect login and password combination');
  });

  it('Login with incorrect password', () => {
    cy.get('input[autocomplete=username]').type('admin');
    cy.get('input[autocomplete=current-password]').type('12345');
    cy.contains('button', 'Log in').should('be.visible').click();
    check.toast('Authentication failed. Incorrect login and password combination');
  });

});
