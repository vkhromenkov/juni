import * as table from "../units/table";
import * as open from "../actions/open";
import { dropDown, panel } from "../units/components";
import { goToMain } from "../actions/goTo";
import { login } from "../actions/login";
import { faker } from '@faker-js/faker';

describe('Main (Home)', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    cy.get('div[data-qaid=mainPage]').should('be.visible');
  });

  it('Draft list', () => {
    // Prepare data
    const draftDesc = faker.string.alpha(5);
    cy.fixture('data/personDraft').then((draft) => {
      draft.description = draftDesc;
      cy.createDraft(draft);
    });
    cy.get('@draftId').then((id) => {
      cy.fixture('data/person').then((person) => {
        person.draftId = id;
        person.dataRecord.externalId.externalId = faker.string.uuid();
        person.dataRecord.simpleAttributes[0].value = faker.person.lastName();
        person.dataRecord.simpleAttributes[1].value = faker.person.firstName();
        cy.createRecord(person);
      });
    });
    // Select Person entity
    panel('Draft list').should('be.visible').find('div[class*=lastItem] div[data-component-id=Select] div[class^=inputValue]').click();
    dropDown().should('be.visible').within(() => {
      cy.get('div[class^=row]').contains('Person').click();
    });
    cy.get('div.ant-spin').should('not.exist');
    // Check draft
    panel('Draft list').should('be.visible').within(() => {
      table.cell(1, 1).should('have.text', draftDesc);
      table.cell(1, 4).should('have.text', 'In progress');
    });
  });

  it('Data widget', () => {
    // Prepare data
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    cy.fixture('data/person').then((person) => {
      person.dataRecord.externalId.externalId = faker.string.uuid();
      person.dataRecord.simpleAttributes[0].value = lastName;
      person.dataRecord.simpleAttributes[1].value = firstName;
      cy.createRecord(person);
    });
    // Open created record to add link into Data widget
    cy.get('@etalonId').then(id => {
      open.recordByEtalonId('person', id);
    });
    cy.get('div.ant-skeleton-content').should('not.exist');
    goToMain();
    cy.reload();
    panel('Data').should('be.visible').contains(`${lastName}, ${firstName}`).should('be.visible');
  });

  it('Main menu pin', () => {
    const menuItemIndex = faker.number.int({ min: 0, max: 15 });
    // Pin random main menu button to header
    cy.get('button[data-qaid=main]').should('be.visible').click();
    cy.get('div[data-qaid=main-menu]').should('be.visible').within(() => {
      cy.get('div[class^=toolbar]>button').click();
      cy.get('a.ud-main-menu-item').eq(menuItemIndex).click();
      cy.get('div[class^=toolbar]>button').click();
      cy.get('a.ud-main-menu-item').eq(menuItemIndex).should('have.class', 'is-pinned');
    });
    cy.get('button[data-qaid=main]').click();
    cy.get('div.ud-pinned-menu').should('be.visible').find('a.ud-main-menu-item.is-pinned').should('have.length.greaterThan', 0);
  });

});
