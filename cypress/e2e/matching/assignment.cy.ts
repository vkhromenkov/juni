import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import { messageDialog, textAreaByLabel, selectByLabel, draftSwitcher, panel, optionList, dropDown } from "../../units/components";
import { goToMatching } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import MatchingPage from '../../po/MatchingPage';

describe('Matching: rules assignment', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToMatching();
  });

  it('Create matching set assignment', () => {
    // Prepare data
    const setName = faker.string.alpha(10);
    const setDName = faker.string.alpha(10).toUpperCase();
    const tableName = faker.string.alpha(10);
    const tableDName = faker.string.alpha(10).toUpperCase();
    const ruleName = faker.string.alpha(10);
    create.matchingTable(tableDName, tableName);
    create.matchingRule(faker.string.alpha(10).toUpperCase(), ruleName);
    create.matchingSet(setDName, tableName, ruleName, setName);
    // Switch to assignment
    cy.get('div[data-component-id=TabBar]').contains('Rules assignment').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add assignment
    panel('Reference sets').within(() => {
      cy.contains('button', 'Add').click();
      dropDown().should('be.visible').contains('All Attr Types Lookup').should('be.visible').click();
    });
    // Fill assignment fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      selectByLabel('Table').should('be.visible').click();
      optionList().should('be.visible').contains(tableDName).click();
      selectByLabel('Table').find('div[class^=displayValue]').should('have.text', tableDName);
      cy.get('div[class^=mappingContainer]').should('be.visible').within(() => {
        textAreaByLabel('Column 1').clear().type('lookup:allAttrTypesLookup:{}.strAttr');
        textAreaByLabel('Column 2').clear().type('lookup:allAttrTypesLookup:{}.strCodeAttr');
        cy.get('div[class^=fieldsContainer]').should('be.visible').within(() => {
          selectByLabel('Rule set 1').should('be.visible').click();
          optionList().should('be.visible').contains(setDName).click();
        });
      });
      cy.contains('button', 'Save').click();
      check.toast('Assignment is saved');
    });
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new MatchingPage().publish();
    // Check created assignment
    panel('Reference sets').within(() => {
      table.rowByText('All Attr Types Lookup').should('be.visible').within(() => {
        cy.get('div[class^=cellContainer]').eq(1).should('have.text', tableDName);
        cy.get('div[class^=cellContainer]').eq(2).should('have.text', setDName);
      });
    });
  });

  it('Delete matching set assignment', () => {
    // Prepare data
    const setName = faker.string.alpha(10);
    const tableName = faker.string.alpha(10);
    const ruleName = faker.string.alpha(10);
    create.matchingTable(faker.string.alpha(10).toUpperCase(), tableName);
    create.matchingRule(faker.string.alpha(10).toUpperCase(), ruleName);
    create.matchingSet(faker.string.alpha(10).toUpperCase(), tableName, ruleName, setName);
    cy.fixture('matching/assignment').then((matching) => {
      matching.assignment.namespace = 'lookup';
      matching.assignment.typeName = 'countries';
      matching.assignment.sets[0] = setName;
      matching.assignment.matchingTables[0] = tableName;
      matching.assignment.mappings[0].matchingTableName = tableName;
      matching.assignment.mappings[0].mappings[0].columnName = 'column1';
      matching.assignment.mappings[0].mappings[0].path = 'lookup:countries:{}.countryCode';
      matching.assignment.mappings[0].mappings[1].columnName = 'column2';
      matching.assignment.mappings[0].mappings[1].path = 'lookup:countries:{}.countryName';
      cy.createMatchingAssignment(matching);
    });
    // Switch to assignment
    cy.get('div[data-component-id=TabBar]').contains('Rules assignment').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Select created assignment
    table.rowByText('Countries').trigger('mouseover').find('input[type=checkbox]').check();
    // Click delete
    cy.contains('button', 'Delete assignment').should('be.visible').click();
    // Click Continue button in confirmation dialog
    messageDialog().should('be.visible').find('button[data-qaid=confirm]').click();
    messageDialog().should('not.exist');
    // Check popup notification message
    check.toast('Assignment is deleted');
    // Publish draft
    new MatchingPage().publish();
    // Check assignment
    panel('Reference sets').within(() => {
      table.rowByText('Countries').should('not.exist');
    });
  });

  it('Matching set assignment: Required fields', () => {
    // Switch to assignment
    cy.get('div[data-component-id=TabBar]').contains('Rules assignment').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add assignment
    panel('Entities').within(() => {
      cy.contains('button', 'Add').click();
      dropDown().should('be.visible').contains('Address').should('be.visible').click();
    });
    // Save empty assignment
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      selectByLabel('Table').should('be.visible').click();
      optionList().should('be.visible').contains('Full Name').click();
      cy.get('div[class^=cardHeader]').contains('button', 'Add').click();
      cy.contains('button', 'Save').click();
      check.toast('Validation error');
      check.fieldErrorText('Table', 'Required field');
    });
  });

});
