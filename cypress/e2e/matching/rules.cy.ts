import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import { textAreaByLabel, inputByLabel, checkboxByLabel, selectByLabel, draftSwitcher, panel, optionList } from "../../units/components";
import { goToMatching } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import MatchingPage from '../../po/MatchingPage';

describe('Matching: rules', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToMatching();
  });

  it('Create matching rule', () => {
    // Prepare data for filling
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha(10).toUpperCase();
    const description = faker.lorem.sentence();
    // Switch to rules
    cy.get('div[data-component-id=TabBar]').contains('Matching rules').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add rule
    cy.contains('button', 'Create rule').should('be.visible').click();
    // Fill table fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      inputByLabel('Name').type(name).should('have.value', name);
      inputByLabel('Display name').type(displayName).should('have.value', displayName);
      textAreaByLabel('Description').type(description).should('have.value', description);
      selectByLabel('Storage').click();
      optionList().should('be.visible').contains('Postgres').click();
      selectByLabel('Storage').find('div[class^=displayValue]').should('have.text', 'Postgres matching storage');
      panel('Algorithms').within(() => {
        selectByLabel('Algorithm').click();
        optionList().should('be.visible').contains('Exact').click();
        selectByLabel('Algorithm').find('div[class^=displayValue]').should('have.text', 'Exact match');
        checkboxByLabel('Case-insensitive').should('be.visible');
      });
      cy.contains('button', 'Save').click();
      check.toast('Rule is saved');
    });
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new MatchingPage().publish();
    // Check created rule
    table.rowByText(name).should('be.visible').within(() => {
      cy.get('div[class^=cellContainer]').eq(0).should('have.text', displayName);
      cy.get('div[class^=cellContainer]').eq(2).should('have.text', description);
      cy.get('div[class^=cellContainer]').eq(3).should('have.text', 'Exact match');
    });
  });

  it('Update matching rule', () => {
    // Prepare data
    const displayName = faker.string.alpha(10).toUpperCase();
    const updDisplayName = faker.string.alpha(10).toUpperCase();
    create.matchingRule(displayName);
    // Switch to rules
    cy.get('div[data-component-id=TabBar]').contains('Matching rules').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Open created rule
    table.rowByText(displayName).click();
    // Update display name and delete one of matching columns
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      inputByLabel('Display name').clear().type(updDisplayName).should('have.value', updDisplayName);
      cy.get('section[class^=itemContainer]').eq(1).find('button').should('be.visible').click();
      cy.contains('button', 'Save').click();
      check.toast('Rule is saved');
    });
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new MatchingPage().publish();
    // Check updated rule
    table.rowByText(updDisplayName).should('be.visible').within(() => {
      cy.get('div[class^=cellContainer]').eq(3).should('have.text', 'Exact match');
    });
    table.rowByText(displayName).should('not.exist');
  });

  it('Delete matching rule', () => {
    // Prepare data
    const displayName = faker.string.alpha(10).toUpperCase();
    create.matchingRule(displayName);
    // Switch to rules
    cy.get('div[data-component-id=TabBar]').contains('Matching rules').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Select created rule
    table.rowByText(displayName).trigger('mouseover').find('input[type=checkbox]').check();
    // Click delete
    cy.contains('button', 'Delete rule').should('be.visible').click();
    // Click Continue button in confirmation dialog
    cy.get('div[data-qaid=confirmbox]>div').should('be.visible').find('button[data-qaid=confirm]').click();
    cy.get('div[data-qaid=confirmbox]').should('not.exist');
    // Check popup notification message
    check.toast('Rule is deleted');
    // Publish draft
    new MatchingPage().publish();
    // Check rule
    table.rowByText(displayName).should('not.exist');
  });

  it('Matching rule: Required fields', () => {
    // Switch to rules
    cy.get('div[data-component-id=TabBar]').contains('Matching rules').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add rule
    cy.contains('button', 'Create rule').click();
    // Fill rule fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      inputByLabel('Name').type(faker.string.alpha(10));
      cy.contains('button', 'Save').click();
      check.toast('Validation error');
      check.fieldErrorText('Display name', 'Display name is required');
    });
  });

});