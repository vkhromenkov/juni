import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import { textAreaByLabel, inputByLabel, selectByLabel, draftSwitcher, panel, optionList } from "../../units/components";
import { goToMatching } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import MatchingPage from '../../po/MatchingPage';

describe('Matching: rule sets', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToMatching();
  });

  it('Create matching rule set', () => {
    // Prepare data
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha(10).toUpperCase();
    const description = faker.lorem.sentence();
    const tableName = faker.string.alpha(10).toUpperCase();
    const ruleName = faker.string.alpha(10).toUpperCase();
    create.matchingTable(tableName);
    create.matchingRule(ruleName);
    // Switch to sets
    cy.get('div[data-component-id=TabBar]').contains('Matching rule sets').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add set
    cy.contains('button', 'Create set').should('be.visible').click();
    // Fill set fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      panel('General').within(() => {
        inputByLabel('Name').type(name).should('have.value', name);
        inputByLabel('Display name').type(displayName).should('have.value', displayName);
        textAreaByLabel('Description').type(description).should('have.value', description);
        selectByLabel('Filter by storage').click();
        optionList().should('be.visible').contains('Postgres').click();
        selectByLabel('Filter by storage').find('div[class^=displayValue]').should('have.text', 'Postgres matching storage');
        selectByLabel('Use table').click();
        optionList().should('be.visible').contains(tableName).click();
        selectByLabel('Use table').find('div[class^=displayValue]').should('have.text', tableName);
      });
      panel('Rules').within(() => {
        selectByLabel('Rule').click();
        optionList().should('be.visible').contains(ruleName).click();
        selectByLabel('Rule').find('div[class^=displayValue]').should('have.text', ruleName);
        selectByLabel('Exact match').eq(0).should('be.visible').click();
        optionList().should('be.visible').contains('Column 1').click();
        selectByLabel('Exact match').eq(0).find('div[class^=displayValue]').should('have.text', 'Column 1');
        selectByLabel('Exact match').eq(1).should('be.visible').click();
        optionList().should('be.visible').contains('Column 2').click();
        selectByLabel('Exact match').eq(1).find('div[class^=displayValue]').should('have.text', 'Column 2');
      });
      cy.contains('button', 'Save').click();
      check.toast('Rule set is saved');
    });
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new MatchingPage().publish();
    // Check created set
    table.rowByText(name).should('be.visible').within(() => {
      cy.get('div[class^=cellContainer]').eq(0).should('have.text', displayName);
      cy.get('div[class^=cellContainer]').eq(2).should('have.text', description);
      cy.get('div[class^=cellContainer]').eq(3).should('have.text', ruleName);
    });
  });

  it('Update matching rule set', () => {
    // Prepare data
    const displayName = faker.string.alpha(10).toUpperCase();
    const updDisplayName = faker.string.alpha(10).toUpperCase();
    const tableName = faker.string.alpha(10);
    const ruleName = faker.string.alpha(10);
    create.matchingTable(tableName.toUpperCase(), tableName);
    create.matchingRule(ruleName.toUpperCase(), ruleName);
    create.matchingSet(displayName, tableName, ruleName);
    // Switch to sets
    cy.get('div[data-component-id=TabBar]').contains('Matching rule sets').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Open created set
    table.rowByText(displayName).click();
    // Update display name
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      panel('General').within(() => {
        inputByLabel('Display name').clear().type(updDisplayName).should('have.value', updDisplayName);
      });
      cy.contains('button', 'Save').click();
      check.toast('Rule set is saved');
    });
    cy.get('div[data-component-id=Drawer]').should('not.exist');
    // Publish draft
    new MatchingPage().publish();
    // Check updated set
    table.rowByText(updDisplayName).should('be.visible');
    table.rowByText(displayName).should('not.exist');
  });

  it('Delete matching rule set', () => {
    // Prepare data
    const displayName = faker.string.alpha(10).toUpperCase();
    const tableName = faker.string.alpha(10);
    const ruleName = faker.string.alpha(10);
    create.matchingTable(tableName.toUpperCase(), tableName);
    create.matchingRule(ruleName.toUpperCase(), ruleName);
    create.matchingSet(displayName, tableName, ruleName);
    // Switch to sets
    cy.get('div[data-component-id=TabBar]').contains('Matching rule sets').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Select created set
    table.rowByText(displayName).trigger('mouseover').find('input[type=checkbox]').check();
    // Click delete
    cy.contains('button', 'Delete set').should('be.visible').click();
    // Click Continue button in confirmation dialog
    cy.get('div[data-qaid=confirmbox]>div').should('be.visible').find('button[data-qaid=confirm]').click();
    cy.get('div[data-qaid=confirmbox]').should('not.exist');
    // Check popup notification message
    check.toast('Rule set is deleted');
    // Publish draft
    new MatchingPage().publish();
    // Check rule
    table.rowByText(displayName).should('not.exist');
  });

  it('Matching rule set: Required fields', () => {
    // Switch to rules
    cy.get('div[data-component-id=TabBar]').contains('Matching rule sets').click();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add rule
    cy.contains('button', 'Create set').click();
    // Fill rule fields and save
    cy.get('div[data-component-id=Drawer]').should('be.visible').within(() => {
      inputByLabel('Name').type(faker.string.alpha(10));
      cy.contains('button', 'Save').click();
      check.toast('Validation error');
      check.fieldErrorText('Display name', 'Display name is required');
      check.fieldErrorText('Filter by storage', 'Required field');
      check.fieldErrorText('Use table', 'Required field');
    });
  });

});