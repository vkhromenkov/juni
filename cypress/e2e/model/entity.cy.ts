import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import {
  headerButton, messageDialog, dropDown, actionsButton, checkboxByLabel, textAreaByLabel,
  inputByLabel, draftSwitcher, fieldByLabel, siderListTree, selectByLabel
} from "../../units/components";
import { goToDataModel } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import ModelPage from '../../po/ModelPage';
import PublishDraftDialog from '../../po/PublishDraftDialog';

describe('Data model: entity', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToDataModel();
  });

  it('Create entity', () => {
    // Prepare data for filling
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const description = faker.lorem.sentence();
    const simpleAttrName = faker.string.alpha(10);
    const simpleAttrDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const simpleAttrDesc = faker.lorem.sentence();
    const complexAttrName = faker.string.alpha(10);
    const complexAttrDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const complexAttrDesc = faker.lorem.sentence();
    const arrayAttrName = faker.string.alpha(10);
    const arrayAttrDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const arrayAttrDesc = faker.lorem.sentence();
    const relRefName = faker.string.alpha(10);
    const relRefDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const relRefAttrName = faker.string.alpha(10);
    const relRefAttrDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const relRefAttrDesc = faker.lorem.sentence();
    const relContainsName = faker.string.alpha(10);
    const relContainsDName = faker.string.alpha({ length: 10, casing: 'upper' });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add new entity
    actionsButton(1).click();
    dropDown().contains('Entity').click();
    // Set name
    inputByLabel('Name').type(name).should('have.value', name);
    // Set display name
    inputByLabel('Display name').type(displayName).should('have.value', displayName);
    // Set description
    textAreaByLabel('Description').type(description).should('have.value', description);
    // Switch to Attributes tab
    cy.get('div[data-component-id=TabBar]').contains('Attributes').click();
    // Add simple attribute
    cy.contains('button', 'Add attribute').click();
    dropDown().contains('Simple attribute').click();
    inputByLabel('Name').type(simpleAttrName).should('have.value', simpleAttrName);
    inputByLabel('Display name').type(simpleAttrDName).should('have.value', simpleAttrDName);
    textAreaByLabel('Description').type(simpleAttrDesc).should('have.value', simpleAttrDesc);
    fieldByLabel('Attribute type').click();
    dropDown().should('be.visible').contains('Simple type').click();
    fieldByLabel('Attribute type').find('div[class^=displayValue]').should('have.text', 'Simple type');
    fieldByLabel('Type').should('be.visible').click();
    dropDown().contains('String').click();
    fieldByLabel('Type').find('div[class^=displayValue]').should('have.text', 'String');
    checkboxByLabel('Searchable').click();
    checkboxByLabel('Main displayable').click();
    // Add complex attribute
    cy.contains('button', 'Add attribute').click();
    dropDown().contains('Complex attribute').click();
    inputByLabel('Name').type(complexAttrName).should('have.value', complexAttrName);
    inputByLabel('Display name').type(complexAttrDName).should('have.value', complexAttrDName);
    textAreaByLabel('Description').type(complexAttrDesc).should('have.value', complexAttrDesc);
    selectByLabel('Nested entity').should('be.visible').click();
    dropDown().contains('Complex Attribute').click();
    fieldByLabel('Nested entity').find('div[class^=displayValue]').should('have.text', 'Complex Attribute');
    // Add array attribute
    cy.contains('button', 'Add attribute').click();
    dropDown().contains('Array attribute').click();
    inputByLabel('Name').type(arrayAttrName).should('have.value', arrayAttrName);
    inputByLabel('Display name').type(arrayAttrDName).should('have.value', arrayAttrDName);
    textAreaByLabel('Description').type(arrayAttrDesc).should('have.value', arrayAttrDesc);
    fieldByLabel('Attribute type').click();
    dropDown().should('be.visible').contains('Simple type').click();
    fieldByLabel('Attribute type').find('div[class^=displayValue]').should('have.text', 'Simple type');
    fieldByLabel('Type').should('be.visible').click();
    dropDown().contains('Integer').click();
    fieldByLabel('Type').find('div[class^=displayValue]').should('have.text', 'Integer');
    checkboxByLabel('Searchable').click();
    // Switch to Relations tab
    cy.get('div[data-component-id=TabBar]').contains('Relations').click();
    // Add reference relation
    cy.contains('button', 'Add relation').click();
    cy.get('div[class^=relationList]').within(() => {
      table.treeRow(1).click();
    });
    cy.get('div[class^=settingsContainer]').within(() => {
      inputByLabel('Name').type(relRefName).should('have.value', relRefName);
      inputByLabel('Display name').type(relRefDName).should('have.value', relRefDName);
      fieldByLabel('Relation type').click();
      dropDown().contains('References').click();
      fieldByLabel('Relation type').find('div[class^=displayValue]').should('have.text', 'References');
      fieldByLabel('Linked entity').click();
      dropDown().contains('Product').click();
      fieldByLabel('Linked entity').find('div[class^=displayValue]').should('have.text', 'Product');
      // Add relation attribute
      cy.get('div[data-component-id=TabBar]').contains('Attributes').click();
      cy.contains('button', 'Add attribute').click();
      dropDown().contains('Simple attribute').click();
      inputByLabel('Name').type(relRefAttrName).should('have.value', relRefAttrName);
      inputByLabel('Display name').type(relRefAttrDName).should('have.value', relRefAttrDName);
      textAreaByLabel('Description').type(relRefAttrDesc).should('have.value', relRefAttrDesc);
      fieldByLabel('Attribute type').click();
      dropDown().contains('Simple type').click();
      fieldByLabel('Attribute type').find('div[class^=displayValue]').should('have.text', 'Simple type');
      fieldByLabel('Type').should('be.visible').click();
      dropDown().contains('Boolean').click();
      fieldByLabel('Type').find('div[class^=displayValue]').should('have.text', 'Boolean');
      checkboxByLabel('Searchable').click();
    });
    // Add contains relation
    cy.contains('button', 'Add relation').click();
    cy.get('div[class^=relationList]').within(() => {
      table.treeRow(2).click();
    });
    cy.get('div[class^=settingsContainer]').within(() => {
      cy.get('div[data-component-id=TabBar]').contains('Settings').click();
      inputByLabel('Name').type(relContainsName).should('have.value', relContainsName);
      inputByLabel('Display name').type(relContainsDName).should('have.value', relContainsDName);
      fieldByLabel('Relation type').click();
      dropDown().contains('Contains').click();
      fieldByLabel('Relation type').find('div[class^=displayValue]').should('have.text', 'Contains');
      fieldByLabel('Linked entity').click();
      dropDown().contains('Address').click();
      fieldByLabel('Linked entity').find('div[class^=displayValue]').should('have.text', 'Address');
      check.tabIsDisabled(cy.get('div[data-component-id=TabBar]').contains('div', 'Attributes'));
    });
    // Switch to Consolidation tab
    cy.get('div[data-component-id=TabBar]').contains('Consolidation').click();
    cy.get('div[class^=attributesList]').within(() => {
      cy.contains('button', 'Add').click();
      dropDown().contains(simpleAttrDName).click();
      cy.contains('button', 'Source list').click();
      dropDown().contains('Select all').click();
      table.row(1).should('be.visible');
      table.cell(1, 2).should('have.text', simpleAttrDName);
    });
    // Save entity
    cy.contains('button', 'Save').click();
    check.toast('Changes in the draft are saved');
    // Publish entity
    new ModelPage().publish();
    siderListTree().should('contains.text', displayName);
  });

  it('Update entity', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const updDisplayName = faker.string.alpha({ length: 10, casing: 'upper' });
    create.entity(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Open created entity
    siderListTree().contains(displayName).scrollIntoView().click();
    // Edit fields
    inputByLabel('Display name').clear().type(updDisplayName).should('have.value', updDisplayName);
    // Save entity
    cy.contains('button', 'Save').click();
    check.toast('Changes in the draft are saved');
    // Publish entity
    new ModelPage().publish();
    siderListTree().should('contains.text', updDisplayName);
    siderListTree().should('not.contain.text', displayName);
  });

  it('Delete entity', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    create.entity(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Open created entity
    siderListTree().contains(displayName).scrollIntoView().click();
    // Delete entity
    headerButton(2).click();
    messageDialog().find('button[data-qaid=confirm]').click();
    check.toast('Changes in the draft are saved');
    // Publish entity
    new ModelPage().publish();
    siderListTree().should('not.contain.text', displayName);
  });

  it('Entity with empty name and display name', () => {
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add new entity
    actionsButton(1).click();
    dropDown().contains('Entity').click();
    // Set description
    textAreaByLabel('Description').type('something');
    // Save entity
    cy.contains('button', 'Save').click();
    check.toast('Validation error');
    // Check errors on fields
    check.fieldErrorText('Name', 'Name is required');
    check.fieldErrorText('Display name', 'Display name is required');
    // Delete entity
    headerButton(2).click();
    messageDialog().find('button[data-qaid=confirm]').click();
    check.toast('Changes in the draft are saved');
    // Publish entity
    actionsButton(2).click();
    new PublishDraftDialog().checkWarningIsDisplayed();
    new PublishDraftDialog().clickConfirm();
    check.toast('Draft is published');
  });

  it('Entity with incorrect name', () => {
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add new entity
    actionsButton(1).click();
    dropDown().contains('Entity').click();
    // Set name
    inputByLabel('Name').type('2 !куроа');
    // Save entity
    cy.contains('button', 'Save').click();
    check.toast('Validation error');
    // Check errors on fields
    check.fieldErrorText('Name', 'The value must contain latin letters, numbers, symbols "-", "_" and start with a letter. Cannot contain spaces.');
    // Delete entity
    headerButton(2).click();
    messageDialog().find('button[data-qaid=confirm]').click();
    check.toast('Changes in the draft are saved');
    // Publish entity
    actionsButton(2).click();
    new PublishDraftDialog().checkWarningIsDisplayed();
    new PublishDraftDialog().clickConfirm();
    check.toast('Draft is published');
  });

});