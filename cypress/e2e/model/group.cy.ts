import * as check from "../../actions/check";
import { messageDialog, draftSwitcher, siderListTree } from "../../units/components";
import { goToDataModel } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import ModelPage from '../../po/ModelPage';

describe('Data model: groups', () => {

  const rootDisplayName = 'Root group';

  beforeEach(() => {
    cy.visit('/');
    login();
    goToDataModel();
  });

  it('Add group', () => {
    // Prepare data for filling
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add new group under root
    siderListTree().contains('div[class^=row_]', rootDisplayName).trigger('mouseover').find('button').click();
    siderListTree().find('div.ud-input input').type(displayName + '{enter}');
    // Publish model
    new ModelPage().publish();
    siderListTree().should('contains.text', displayName);
  });

  it('Edit group', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const updDisplayName = faker.string.alpha({ length: 10, casing: 'upper' });
    cy.createGroup(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Update group display name
    siderListTree().contains('div[class^=row_]', displayName).trigger('mouseover').find('button').eq(0).click();
    siderListTree().find('div.ud-input input').clear().type(updDisplayName + '{enter}');
    // Publish model
    new ModelPage().publish();
    siderListTree().should('not.contains.text', displayName);
    siderListTree().should('contains.text', updDisplayName);
  });

  it('Delete group', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    cy.createGroup(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Delete group
    siderListTree().contains('div[class^=row_]', displayName).trigger('mouseover').find('button').eq(2).click();
    messageDialog().find('button[data-qaid=confirm]').click();
    // Publish model
    new ModelPage().publish();
    siderListTree().should('not.contains.text', displayName);
  });

});
