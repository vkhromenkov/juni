import * as check from "../../actions/check";
import * as table from "../../units/table";
import * as create from "../../actions/create";
import {
  headerButton, messageDialog, dropDown, actionsButton, checkboxByLabel, textAreaByLabel,
  inputByLabel, draftSwitcher, fieldByLabel, siderListTree
} from "../../units/components";
import { goToDataModel } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import ModelPage from '../../po/ModelPage';

describe('Data model: lookup', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToDataModel();
  });

  it('Create lookup', () => {
    // Prepare data for filling
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const description = faker.lorem.sentence();
    const codeAttrName = faker.string.alpha(10);
    const codeAttrDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const codeAttrDesc = faker.lorem.sentence();
    const simpleAttrName = faker.string.alpha(10);
    const simpleAttrDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const simpleAttrDesc = faker.lorem.sentence();
    const aliasCodeAttrName = faker.string.alpha(10);
    const aliasCodeAttrDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const aliasCodeAttrDesc = faker.lorem.sentence();
    const arrayAttrName = faker.string.alpha(10);
    const arrayAttrDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const arrayAttrDesc = faker.lorem.sentence();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Add new lookup
    actionsButton(1).click();
    dropDown().contains('Reference set').click();
    // Set name
    inputByLabel('Name').type(name).should('have.value', name);
    // Set display name
    inputByLabel('Display name').type(displayName).should('have.value', displayName);
    // Set description
    textAreaByLabel('Description').type(description).should('have.value', description);
    // Switch to Attributes tab
    cy.get('div[data-component-id=TabBar]').contains('Attributes').click();
    // Fill code attribute
    cy.get('div[class^=childContainer]').first().within(() => {
      table.treeRow(1).click();
    });
    inputByLabel('Name').type(codeAttrName).should('have.value', codeAttrName);
    inputByLabel('Display name').type(codeAttrDName).should('have.value', codeAttrDName);
    textAreaByLabel('Description').type(codeAttrDesc).should('have.value', codeAttrDesc);
    fieldByLabel('Attribute type').find('div[class^=displayValue]').should('have.text', 'Simple type');
    fieldByLabel('Type').should('be.visible').click();
    dropDown().contains('String').click();
    fieldByLabel('Type').should('contains.text', 'String');
    checkboxByLabel('Main displayable').click();
    // Add simple attribute
    cy.contains('button', 'Add attribute').click();
    dropDown().contains('Simple attribute').click();
    inputByLabel('Name').type(simpleAttrName).should('have.value', simpleAttrName);
    inputByLabel('Display name').type(simpleAttrDName).should('have.value', simpleAttrDName);
    textAreaByLabel('Description').type(simpleAttrDesc).should('have.value', simpleAttrDesc);
    fieldByLabel('Attribute type').click();
    dropDown().should('be.visible').contains('Simple type').click();
    fieldByLabel('Attribute type').find('div[class^=displayValue]').should('have.text', 'Simple type');
    fieldByLabel('Type').should('be.visible').click();
    dropDown().contains('String').click();
    fieldByLabel('Type').find('div[class^=displayValue]').should('have.text', 'String');
    checkboxByLabel('Searchable').click();
    checkboxByLabel('Main displayable').click();
    // Add array attribute
    cy.contains('button', 'Add attribute').click();
    dropDown().contains('Array attribute').click();
    inputByLabel('Name').type(arrayAttrName).should('have.value', arrayAttrName);
    inputByLabel('Display name').type(arrayAttrDName).should('have.value', arrayAttrDName);
    textAreaByLabel('Description').type(arrayAttrDesc).should('have.value', arrayAttrDesc);
    fieldByLabel('Attribute type').click();
    dropDown().should('be.visible').contains('Simple type').click();
    fieldByLabel('Attribute type').find('div[class^=displayValue]').should('have.text', 'Simple type');
    fieldByLabel('Type').should('be.visible').click();
    dropDown().contains('Number').click();
    fieldByLabel('Type').find('div[class^=displayValue]').should('have.text', 'Number');
    checkboxByLabel('Searchable').click();
    // Add alias code attribute
    cy.contains('button', 'Add attribute').click();
    dropDown().contains('Alias code attribute').click();
    inputByLabel('Name').type(aliasCodeAttrName).should('have.value', aliasCodeAttrName);
    inputByLabel('Display name').type(aliasCodeAttrDName).should('have.value', aliasCodeAttrDName);
    textAreaByLabel('Description').type(aliasCodeAttrDesc).should('have.value', aliasCodeAttrDesc);
    fieldByLabel('Attribute type').find('div[class^=displayValue]').should('have.text', 'Simple type');
    fieldByLabel('Type').should('be.visible').click();
    dropDown().contains('Integer').click();
    fieldByLabel('Type').should('contains.text', 'Integer');
    // Save lookup
    cy.contains('button', 'Save').click();
    check.toast('Changes in the draft are saved');
    // Publish lookup
    new ModelPage().publish();
    siderListTree().should('contains.text', displayName);
  });

  it('Update lookup', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const updDisplayName = faker.string.alpha({ length: 10, casing: 'upper' });
    create.lookup(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Open created lookup
    siderListTree().contains(displayName).scrollIntoView().click();
    // Edit fields
    inputByLabel('Display name').clear().type(updDisplayName).should('have.value', updDisplayName);
    // Save lookup
    cy.contains('button', 'Save').click();
    check.toast('Changes in the draft are saved');
    // Publish lookup
    new ModelPage().publish();
    siderListTree().should('contains.text', updDisplayName);
    siderListTree().should('not.contain.text', displayName);
  });

  it('Delete lookup', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    create.lookup(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Open created lookup
    siderListTree().contains(displayName).scrollIntoView().click();
    // Delete lookup
    headerButton(2).click();
    messageDialog().find('button[data-qaid=confirm]').click();
    check.toast('Changes in the draft are saved');
    // Publish lookup
    new ModelPage().publish();
    siderListTree().should('not.contain.text', displayName);
  });

});