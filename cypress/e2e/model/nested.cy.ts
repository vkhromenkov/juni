import * as check from "../../actions/check";
import * as create from "../../actions/create";
import {
  headerButton, messageDialog, dropDown, actionsButton, checkboxByLabel, textAreaByLabel,
  inputByLabel, draftSwitcher, fieldByLabel, siderListTree, selectByLabel
} from "../../units/components";
import { goToDataModel } from "../../actions/goTo";
import { login } from "../../actions/login";
import { faker } from '@faker-js/faker';
import ModelPage from '../../po/ModelPage';

describe('Data model: nested entity', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToDataModel();
  });

  it('Create nested entity', () => {
    // Prepare data for filling
    const name = faker.string.alpha(10);
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const description = faker.lorem.sentence();
    const simpleAttrName = faker.string.alpha(10);
    const simpleAttrDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const simpleAttrDesc = faker.lorem.sentence();
    const complexAttrName = faker.string.alpha(10);
    const complexAttrDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const complexAttrDesc = faker.lorem.sentence();
    const arrayAttrName = faker.string.alpha(10);
    const arrayAttrDName = faker.string.alpha({ length: 10, casing: 'upper' });
    const arrayAttrDesc = faker.lorem.sentence();
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Switch to Nested entities
    cy.contains('button', 'Nested entities').click();
    // Add new nested entity
    actionsButton(1).click();
    dropDown().contains('Nested entity').click();
    // Set name
    inputByLabel('Name').type(name).should('have.value', name);
    // Set display name
    inputByLabel('Display name').type(displayName).should('have.value', displayName);
    // Set description
    textAreaByLabel('Description').type(description).should('have.value', description);
    // Switch to Attributes tab
    cy.get('div[data-component-id=TabBar]').contains('Attributes').click();
    // Add simple attribute
    cy.contains('button', 'Add attribute').click();
    dropDown().contains('Simple attribute').click();
    inputByLabel('Name').type(simpleAttrName).should('have.value', simpleAttrName);
    inputByLabel('Display name').type(simpleAttrDName).should('have.value', simpleAttrDName);
    textAreaByLabel('Description').type(simpleAttrDesc).should('have.value', simpleAttrDesc);
    fieldByLabel('Attribute type').click();
    dropDown().should('be.visible').contains('Simple type').click();
    fieldByLabel('Attribute type').find('div[class^=displayValue]').should('have.text', 'Simple type');
    fieldByLabel('Type').should('be.visible').click();
    dropDown().contains('String').click();
    fieldByLabel('Type').find('div[class^=displayValue]').should('have.text', 'String');
    checkboxByLabel('Searchable').click();
    // Add complex attribute
    cy.contains('button', 'Add attribute').click();
    dropDown().contains('Complex attribute').click();
    inputByLabel('Name').type(complexAttrName).should('have.value', complexAttrName);
    inputByLabel('Display name').type(complexAttrDName).should('have.value', complexAttrDName);
    textAreaByLabel('Description').type(complexAttrDesc).should('have.value', complexAttrDesc);
    selectByLabel('Nested entity').should('be.visible').click();
    dropDown().contains('Complex Attribute').click();
    fieldByLabel('Nested entity').find('div[class^=displayValue]').should('have.text', 'Complex Attribute');
    // Add array attribute
    cy.contains('button', 'Add attribute').click();
    dropDown().contains('Array attribute').click();
    inputByLabel('Name').type(arrayAttrName).should('have.value', arrayAttrName);
    inputByLabel('Display name').type(arrayAttrDName).should('have.value', arrayAttrDName);
    textAreaByLabel('Description').type(arrayAttrDesc).should('have.value', arrayAttrDesc);
    fieldByLabel('Attribute type').click();
    dropDown().should('be.visible').contains('Simple type').click();
    fieldByLabel('Attribute type').find('div[class^=displayValue]').should('have.text', 'Simple type');
    fieldByLabel('Type').should('be.visible').click();
    dropDown().contains('Integer').click();
    fieldByLabel('Type').find('div[class^=displayValue]').should('have.text', 'Integer');
    checkboxByLabel('Searchable').click();
    // Save nested entity
    cy.contains('button', 'Save').click();
    check.toast('Changes in the draft are saved');
    // Publish nested entity
    new ModelPage().publish();
    siderListTree().should('contains.text', displayName);
  });

  it('Edit nested entity', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    const updDisplayName = faker.string.alpha({ length: 10, casing: 'upper' });
    create.nested(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Switch to Nested entities
    cy.contains('button', 'Nested entities').click();
    // Open created nested entity
    siderListTree().contains(displayName).scrollIntoView().click();
    // Edit fields
    inputByLabel('Display name').clear().type(updDisplayName).should('have.value', updDisplayName);
    // Save nested entity
    cy.contains('button', 'Save').click();
    check.toast('Changes in the draft are saved');
    // Publish nested entity
    new ModelPage().publish();
    siderListTree().should('contains.text', updDisplayName);
    siderListTree().should('not.contain.text', displayName);
  });

  it('Delete nested entity', () => {
    // Prepare data
    const displayName = faker.string.alpha({ length: 10, casing: 'upper' });
    create.nested(displayName);
    // Switch to draft mode
    draftSwitcher().click();
    check.toast('Draft is saved');
    // Switch to Nested entities
    cy.contains('button', 'Nested entities').click();
    // Open created nested entity
    siderListTree().contains(displayName).scrollIntoView().click();
    // Delete nested entity
    headerButton(2).click();
    messageDialog().find('button[data-qaid=confirm]').click();
    check.toast('Changes in the draft are saved');
    // Publish nested entity
    new ModelPage().publish();
    siderListTree().should('not.contain.text', displayName);
  });

});