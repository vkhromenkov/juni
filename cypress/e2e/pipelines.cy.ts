import * as check from "../actions/check";
import { panel, headerButton, modalWindow, siderList, actionsButton, messageDialog, editor } from "../units/components";
import { goToPipeline } from "../actions/goTo";
import { login } from "../actions/login";

describe('Pipelines', () => {

  const SUBJECT_ID = 'juni';

  beforeEach(() => {
    cy.deletePipeline('org.unidata.mdm.data%5BMODEL_REFRESH_START%5D/juni');
    cy.visit('/');
    login();
  });

  it('Create pipeline', () => {
    goToPipeline();
    // Add pipeline
    actionsButton(1).click();
    // Select start segment
    modalWindow('Select start segment').should('be.visible').contains('MODEL_REFRESH_START').click();
    modalWindow('Select segment').should('not.exist');
    panel('START').should('be.visible');
    // Set subject name
    cy.get('input[placeholder="Enter subject name"]').type(SUBJECT_ID).should('have.value', SUBJECT_ID);
    // Add point segment
    cy.contains('button', 'Add segment').should('be.visible').click();
    modalWindow('Select segment').should('be.visible').contains('MODEL_REFRESH_POINT').click();
    modalWindow('Select segment').should('not.exist');
    panel('POINT').should('be.visible');
    cy.wait(500);
    // Select finish segment
    panel('FINISH').should('be.visible').contains('button', 'Select segment').click();
    modalWindow('Select end segment').should('be.visible').contains('MODEL_REFRESH_FINISH').click();
    modalWindow('Select end segment').should('not.exist');
    // Click Save
    cy.contains('button', 'Save').click();
    check.toast('Pipeline is saved');
    // Check pipeline
    panel('START').should('contains.text', 'MODEL_REFRESH_START');
    panel('CONNECTOR').should('not.exist');
    panel('POINT').should('contains.text', 'MODEL_REFRESH_POINT');
    panel('FALLBACK').should('contains.text', 'Segment is empty');
    panel('FINISH').should('contains.text', 'MODEL_REFRESH_FINISH');
    cy.get('div[class^=siderListContainer] input').click().type(SUBJECT_ID);
    siderList().should('contains.text', SUBJECT_ID);
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Edit pipeline', () => {
    // Create new pipeline using REST API
    cy.fixture('pipeline').then((pipeline) => {
      cy.createPipeline(pipeline);
    });
    goToPipeline();
    // Open created pipeline
    cy.get('div[class^=siderListContainer] input').click().type(SUBJECT_ID);
    siderList().contains(SUBJECT_ID).click();
    // Add block
    panel('FALLBACK').find('button').click();
    // Click Save
    cy.contains('button', 'Save').click();
    check.toast('Pipeline is saved');
    // Check pipeline
    panel('START').should('contains.text', 'MODEL_REFRESH_START');
    panel('CONNECTOR').should('not.exist');
    panel('POINT').should('contains.text', 'MODEL_REFRESH_POINT');
    panel('FALLBACK').should('contains.text', 'Segment is empty');
    panel('FINISH').should('contains.text', 'MODEL_REFRESH_FINISH');
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Delete pipeline', () => {
    // Create new pipeline using REST API
    cy.fixture('pipeline').then((pipeline) => {
      cy.createPipeline(pipeline);
    });
    goToPipeline();
    // Open created pipeline
    cy.get('div[class^=siderListContainer] input').click().type(SUBJECT_ID);
    siderList().contains(SUBJECT_ID).click();
    // Click delete button
    headerButton(2).click();
    // Confirmation box should appear
    messageDialog().should('be.visible');
    // Click Continue button
    messageDialog().find('button[data-qaid=confirm]').click();
    // Check popup notification message
    check.toast('Pipeline is deleted');
    // Check pipeline
    cy.get('div[class^=siderListContainer] input').click().clear().type(SUBJECT_ID);
    siderList().should('not.contains.text', SUBJECT_ID);
    // Check editor is closed
    editor('Pipeline').should('not.exist');
  });

  it('Empty segment', () => {
    goToPipeline();
    // Add pipeline
    actionsButton(1).click();
    // Select start segment
    modalWindow('Select start segment').should('be.visible').contains('MODEL_REFRESH_START').click();
    // Set subject name
    cy.get('input[placeholder="Enter subject name"]').type(SUBJECT_ID).should('have.value', SUBJECT_ID);
    // Click Save
    cy.contains('button', 'Save').click();
    check.toast('Invalid number of segments. The pipeline must contain at least 2 points of the \'start\' and \'finish\' types');
  });

  it('Pipeline: Save button state', () => {
    goToPipeline();
    // Click Add button
    actionsButton(1).click();
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
    // Select start segment
    modalWindow('Select start segment').should('be.visible').contains('MODEL_REFRESH_START').click();
    // Check Save button is enabled
    check.buttonIsEnabled(cy.contains('button', 'Save'));
    // Delete segment
    panel('START').find('div[class^=buttonContainer]>button').click();
    modalWindow('Select start segment').should('be.visible').find('div[data-qaid=tools-close]').click();
    modalWindow('Select start segment').should('not.exist');
    // Check Save button is disabled again
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

});
