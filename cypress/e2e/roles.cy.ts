import * as check from "../actions/check";
import { headerButton, siderList, messageDialog, inputByName, actionsButton, editor } from "../units/components";
import { goToRoles } from "../actions/goTo";
import { login } from "../actions/login";
import { faker } from '@faker-js/faker';

describe('Roles', () => {

  const updName = faker.string.alpha(7);
  const updDName = faker.string.alpha(10).toUpperCase();
  const delName = faker.string.alpha(7);
  const delDName = faker.string.alpha(10).toUpperCase();

  before(() => {
    cy.createRole(updName, updDName);
    cy.createRole(delName, delDName);
  });

  beforeEach(() => {
    cy.visit('/');
    login();
    goToRoles();
  });

  it('Create role', () => {
    // Click Add Role button
    actionsButton(1).click();
    let roleName = faker.string.alpha(7);
    let roleDisplayName = faker.string.alpha(10).toUpperCase();
    // Fill role fields
    inputByName('name').should('be.visible').type(roleName).should('have.value', roleName);
    inputByName('displayName').should('be.visible').type(roleDisplayName).should('have.value', roleDisplayName);
    // Check role display name
    cy.get('div[class^=cellContainer]').contains(roleDisplayName).should('be.visible');
    // Click Save
    cy.contains('button', 'Save').should('be.visible').click();
    // Check popup notification message
    check.toast('Role saved');
    // Check role display name
    cy.get('div[class^=cellContainer]').contains(roleDisplayName).should('be.visible');
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Update role', () => {
    // Select created role
    siderList().contains(updDName).click();
    let newDisplayName = faker.string.alpha(10).toUpperCase();
    // Fill new values
    inputByName('displayName').clear().type(newDisplayName).should('have.value', newDisplayName);
    // Check role updated display name
    cy.get('div[class^=cellContainer]').contains(newDisplayName).should('be.visible');
    // Click Save
    cy.contains('button', 'Save').should('be.visible').click();
    // Check popup notification message
    check.toast('Role saved');
    // Check role updated display name
    cy.get('div[class^=cellContainer]').contains(newDisplayName).should('be.visible');
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Delete role', () => {
    // Select created role
    siderList().contains(delDName).click();
    // Click Delete
    headerButton(2).click();
    // Confirmation box should appear
    messageDialog().should('be.visible');
    // Click Continue button
    messageDialog().find('button[data-qaid=confirm]').click();
    // Check popup notification message
    check.toast('Role deleted');
    // Check role disappear from list
    siderList().should('not.contain.text', delDName);
    // Check editor is closed
    editor('Roles').should('not.exist');
  });

  it('Role: Save button state', () => {
    // Click Add Role button
    actionsButton(1).click();
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
    // Fill some property  
    inputByName('name').type('something');
    // Check Save button is enabled
    check.buttonIsEnabled(cy.contains('button', 'Save'));
    // Clear the value  
    inputByName('name').clear();
    // Check Save button is disabled again
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Role: Required fields', () => {
    // Click Add Role button
    actionsButton(1).click();
    // Fill some property
    inputByName('name').type('something');
    // Click Save
    cy.contains('button', 'Save').should('be.visible').click();
    // Check popup notification message
    check.toast('Validation error');
  });

});
