import * as check from "../actions/check";
import * as format from "../actions/format";
import * as create from "../actions/create";
import * as select from "../actions/select";
import { goToData, goToMain } from "../actions/goTo";
import { login } from "../actions/login";
import { modalWindowDialog, modalWindow, panel, dropDown } from "../units/components";
import { faker } from '@faker-js/faker';
import * as joda from '@js-joda/core';


describe('Search', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
    goToData();
  });

  it('Search by attributes', () => {
    // Prepare data
    const firstName = faker.person.firstName();
    const lastName = faker.person.lastName();
    const queryName = faker.string.alpha(5);
    create.person(firstName, lastName);
    // Select Person entity
    select.entityInSearch('Person');
    cy.get('div[class*=udTableCommon]').should('be.visible');
    // Add search criteria
    cy.get('button[data-qaid=addCriteria]').click();
    cy.get('div[data-qaid=criteriaTreeDropDownContainer]').should('be.visible').within(() => {
      cy.get('div[data-qaid=menu_attribute]>div').click();
    });
    cy.get('div[data-qaid=popover_attribute]').should('be.visible').within(() => {
      cy.get('div[data-qaid=item_firstName]').should('be.visible').click();
      cy.get('div[data-qaid=item_lastName]').should('be.visible').click();
    });
    // Fill search attributes values
    cy.get('div[class^=tagsList] div[data-qaid=lastName]').click();
    cy.get('div[data-qaid=modal_lastName]').should('be.visible').within(() => {
      cy.get('div[data-qaid=value] input').type(lastName).should('have.value', lastName);
    });
    cy.get('div[class^=tagsList] div[data-qaid=firstName]').click();
    cy.get('div[data-qaid=modal_firstName]').should('be.visible').within(() => {
      cy.get('div[data-qaid=value] input').type(firstName).should('have.value', firstName);
    });
    // Save search query
    cy.get('button[data-qaid=saveButton]').should('be.visible').click();
    modalWindow('Save a search query').should('be.visible').within(() => {
      cy.get('input[type=text]').should('be.visible').type(queryName).should('have.value', queryName);
      cy.contains('button', 'Save').should('be.visible').click();
    });
    modalWindow('Save a search query').should('not.exist');
    check.toast('Search query saved');
    // Select other entity (e.g. Organization) to check search query
    cy.get('div[class^=entityList] div[data-component-id=Select] div[class^=inputValue]').click();
    dropDown().should('be.visible').within(() => {
      cy.get('div[class^=row]').contains('Organization').click();
    });
    goToMain();
    panel('User search queries').contains(queryName).click();
    cy.get('div[data-qaid=searchPage]').should('be.visible');
    // Check number of records found
    check.recordsCountInSearchIs(1);
    // Check first record attributes
    check.cellText(1, 'First Name', firstName);
    check.cellText(1, 'Last Name', lastName);
  });

  it('Search by search text', () => {
    // Prepare data
    const firstName = faker.string.alpha(10);
    const lastName = faker.string.alpha(10);
    create.person(firstName, lastName);
    // Select Person entity
    select.entityInSearch('Person');
    cy.get('div[class*=udTableCommon]').should('be.visible');
    // Set search text to last name in lower case and run search
    cy.get('input[data-qaid=querySearch]').type(lastName.toLowerCase()).type('{enter}');
    // Check number of records found
    check.recordsCountInSearchIs(1);
    // Check first record attributes
    check.cellText(1, 'First Name', firstName);
    check.cellText(1, 'Last Name', lastName);
    // Set search text to first name in upper case and run search
    cy.get('input[data-qaid=querySearch]').clear().type(firstName.toUpperCase()).type('{enter}');
    // Check number of records found
    check.recordsCountInSearchIs(1);
    // Check first record attributes
    check.cellText(1, 'First Name', firstName);
    check.cellText(1, 'Last Name', lastName);
  });

  it('Search for date', () => {
    // Prepare data
    const orgName = faker.company.name();
    const currentDate = joda.LocalDate.now();
    const fromDateTime = currentDate.minusMonths(1).atStartOfDay();
    const toDateTime = currentDate.plusMonths(1).plusDays(1).atStartOfDay().minus(joda.Duration.ofMillis(1));
    cy.fixture('data/organization').then((org) => {
      org.dataRecord.externalId.externalId = faker.string.uuid();
      org.dataRecord.simpleAttributes[0].value = orgName;
      org.dataRecord.validFrom = format.dateForRestApi(fromDateTime);
      org.dataRecord.validTo = format.dateForRestApi(toDateTime);
      cy.createRecord(org);
    });
    // Select Organization entity
    select.entityInSearch('Organization');
    cy.get('div[class*=udTableCommon]').should('be.visible');
    // Add search criteria
    cy.get('button[data-qaid=addCriteria]').click();
    cy.get('div[data-qaid=criteriaTreeDropDownContainer]').should('be.visible').within(() => {
      cy.get('div[data-qaid=item_actual]').click();
      cy.get('div[data-qaid=menu_attribute]>div').click();
    });
    cy.get('div[data-qaid=popover_attribute]').should('be.visible').within(() => {
      cy.get('div[data-qaid=item_orgName]').click();
    });
    // Fill search attribute value
    cy.get('div[class^=tagsList] div[data-qaid=orgName]').click();
    cy.get('div[data-qaid=modal_orgName]').should('be.visible').within(() => {
      cy.get('div[data-qaid=value] input').type(orgName).should('have.value', orgName);
    });
    // Fill date inside record timeline
    cy.get('div[class^=tagsList] div[data-qaid=actual]').click();
    cy.get('div[data-qaid=modal_actual]').should('be.visible').within(() => {
      cy.contains('button', 'Today').click();
    });
    // Run search
    cy.get('button[data-qaid=searchButton]').click();
    // Check number of records found
    check.recordsCountInSearchIs(1);
    // Check first record attributes
    check.cellText(1, 'Name', orgName);
    // Fill date outside record timeline in the past
    cy.get('div[class^=tagsList] div[data-qaid=actual]').click();
    cy.get('div[data-qaid=modal_actual]').should('be.visible').within(() => {
      cy.get("input[placeholder='Start date']").should('be.enabled').click().clear();
      cy.get("input[placeholder='End date']").should('be.enabled').click().clear();
      cy.get("input[placeholder='Start date']").click().type(format.date(fromDateTime.minusMonths(1)));
      cy.get("input[placeholder='End date']").click().type(format.date(fromDateTime.minusMonths(1))).type('{enter}');
    });
    // Run search
    cy.get('button[data-qaid=searchButton]').click();
    // Check number of records found
    check.recordsCountInSearchIs(0);
    // Check first record attributes
    cy.get('div[data-component-id=Table]').within(() => {
      cy.get('div[class^=rowContainer]').should('not.exist');
    });
    // Fill date outside record timeline in the future
    cy.get('div[class^=tagsList] div[data-qaid=actual]').click();
    cy.get('div[data-qaid=modal_actual]').should('be.visible').within(() => {
      cy.get("input[placeholder='Start date']").should('be.enabled').click().clear();
      cy.get("input[placeholder='End date']").should('be.enabled').click().clear();
      cy.get("input[placeholder='Start date']").click().type(format.date(toDateTime.plusMonths(1)));
      cy.get("input[placeholder='End date']").click().type(format.date(toDateTime.plusMonths(1))).type('{enter}');
    });
    // Run search
    cy.get('button[data-qaid=searchButton]').click();
    // Check number of records found
    check.recordsCountInSearchIs(0);
    // Check first record attributes
    cy.get('div[data-component-id=Table]').within(() => {
      cy.get('div[class^=rowContainer]').should('not.exist');
    });
  });

  it('Search by relation', () => {
    // Prepare data
    const firstName = faker.person.firstName();
    const lastName = faker.string.alpha(10);
    // Create 'right end' record for relation
    const autoModel = faker.string.alpha(5);
    cy.fixture('data/automobile').then((auto) => {
      auto.dataRecord.externalId.externalId = faker.string.uuid();
      auto.dataRecord.simpleAttributes[0].value = autoModel;
      cy.createRecord(auto);
    });
    // Create new record using REST API
    cy.get('@etalonId').then(relRecordId => {
      cy.fixture('data/personWithCar').then((personWithCar) => {
        personWithCar.dataRecord.externalId.externalId = faker.string.uuid();
        personWithCar.dataRecord.simpleAttributes[0].value = lastName;
        personWithCar.dataRecord.simpleAttributes[1].value = firstName;
        personWithCar.relationManyToMany.toUpdate[0].etalonIdTo = relRecordId;
        personWithCar.relationManyToMany.toUpdate[0].simpleAttributes[0].value = faker.number.int({ min: 2000, max: 2030 });
        personWithCar.relationManyToMany.toUpdate[0].simpleAttributes[1].value = faker.number.int();
        cy.createRecord(personWithCar);
      });
    });
    // Select Person entity
    select.entityInSearch('Person');
    cy.get('div[class*=udTableCommon]').should('be.visible');
    // Add search criteria
    cy.get('button[data-qaid=addCriteria]').click();
    cy.get('div[data-qaid=criteriaTreeDropDownContainer]').should('be.visible').within(() => {
      cy.get('div[data-qaid=menu_relations]').click();
    });
    cy.get('div[data-qaid=popover_relations').should('be.visible').within(() => {
      cy.get('div[data-qaid=menu_outgoingRelations]').click();
    });
    cy.get('div[data-qaid=popover_outgoingRelations').should('be.visible').within(() => {
      cy.get('div[data-qaid$=person_auto_rel]').click();
    });
    // Open 'right end' record for relation
    cy.get('div[class^=tagsList] div[data-qaid$=person_auto_rel]').click();
    dropDown().should('be.visible').contains('Choose related records').click();
    cy.get('div[data-qaid=modal] div[class^=rowContainer]:first-child').should('be.visible');
    // Find 'right end' record
    modalWindowDialog().find('input[placeholder^=Search]').should('be.visible').type(autoModel.toLowerCase()).type('{enter}');
    // Select 'right end' record checkbox and confirm
    cy.get('div[data-qaid=modal] div[class^=rowContainer]:first-child').within(() => {
      cy.get('div[class^=checkboxContainer]').click();
    });
    cy.get('button[data-qaid=confirmButton]').should('be.visible').click();
    // Run search
    cy.get('button[data-qaid=searchButton]').click();
    // Check number of records found
    check.recordsCountInSearchIs(1);
    // Check record attributes
    check.cellText(1, 'First Name', firstName);
    check.cellText(1, 'Last Name', lastName);
  });

});
