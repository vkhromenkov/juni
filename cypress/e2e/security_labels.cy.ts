import * as check from "../actions/check";
import { fieldByLabel, headerButton, siderList, messageDialog, inputByLabel, textAreaByLabel, actionsButton, editor, optionList } from "../units/components";
import { goToLabels } from "../actions/goTo";
import { login } from "../actions/login";
import { faker } from '@faker-js/faker';

describe('Security labels', () => {

  const updName = faker.string.alpha(7);
  const updDName = faker.string.alpha(10).toUpperCase();
  const delName = faker.string.alpha(7);
  const delDName = faker.string.alpha(10).toUpperCase();

  before(() => {
    cy.createLabel(updName, updDName);
    cy.createLabel(delName, delDName);
  });

  beforeEach(() => {
    cy.visit('/');
    login();
    goToLabels();
  });

  it('Create security label', () => {
    // Click Add Label button
    actionsButton(1).click();
    const labelName = faker.string.alpha(7);
    const labelDisplayName = faker.string.alpha(10).toUpperCase();
    const description = faker.lorem.sentence();
    // Fill label fields
    inputByLabel('Name').should('be.visible').type(labelName).should('have.value', labelName);
    inputByLabel('Display name').should('be.visible').type(labelDisplayName).should('have.value', labelDisplayName);
    textAreaByLabel('Description').should('be.visible').type(description).should('have.value', description);
    // Fill Entity field
    fieldByLabel('Model object').click();
    cy.get('div[data-qaid=popover_entitytree]').should('be.visible').contains('Person').click();
    // Fill Attributes field
    fieldByLabel('Attributes').click();
    optionList().should('be.visible').contains('Citizenship').click();
    fieldByLabel('Attributes').find('div[class^=inputValue]').click();
    // Check label appears in list
    cy.get('div[class^=cellContainer]').contains(labelDisplayName).should('be.visible');
    // Click Save
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Security label is saved');
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Update security label', () => {
    // Select created label
    siderList().contains(updDName).click();
    const newDisplayName = faker.string.alpha(10).toUpperCase();
    // Fill new values
    inputByLabel('Display name').clear().type(newDisplayName).should('have.value', newDisplayName);
    // Click Save
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Security label is saved');
    // Check display name is updated in label list
    cy.get('div[class^=cellContainer]').contains(newDisplayName).should('be.visible');
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Delete security label', () => {
    // Select created label
    siderList().contains(delDName).click();
    // Click Delete
    headerButton(2).click();
    // Confirmation box should appear
    messageDialog().should('be.visible');
    // Click Continue button
    messageDialog().find('button[data-qaid=confirm]').click();
    // Check popup notification message
    check.toast('Security label is deleted');
    // Check label disappear from list
    siderList().should('not.contain.text', delDName);
    // Check editor is closed
    editor('Security Labels').should('not.exist');
  });

  it('Security label: Save button state', () => {
    // Click Add Label button
    actionsButton(1).click();
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
    // Fill some property
    inputByLabel('Display name').type('something');
    // Check Save button is enabled
    check.buttonIsEnabled(cy.contains('button', 'Save'));
    // Clear the value
    inputByLabel('Display name').clear();
    // Check Save button is disabled again
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Security label: Required fields', () => {
    // Click Add Label button
    actionsButton(1).click();
    // Fill some property
    textAreaByLabel('Description').type('something');
    // Click Save
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Validation error');
    // Checking for errors
    check.fieldErrorText('Name', 'Name is required');
    check.fieldErrorText('Display name', 'Display name is required');
    check.fieldErrorText('Model object', 'Model object not set');
    check.fieldErrorText('Attributes', 'Attributes aren\'t set');
  });

});
