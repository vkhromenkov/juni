import * as check from "../actions/check";
import { headerButton, messageDialog, actionsButton, siderList, inputByLabel, textAreaByLabel, editor } from "../units/components";
import { goToSourceSystems } from "../actions/goTo";
import { login } from "../actions/login";
import { faker } from '@faker-js/faker';

describe('Source systems', () => {

  beforeEach(() => {
    cy.visit('/');
    login();
  });

  it('Create source system', () => {
    goToSourceSystems();
    // Prepare data for filling
    const name = faker.string.alpha(10);
    const description = faker.lorem.sentence();
    const weight = faker.number.int({ min: 0, max: 100 }).toString();
    // Click Add source system
    actionsButton(1).click();
    // Fill source system fields
    inputByLabel('Name').type(name);
    textAreaByLabel('Description').type(description);
    inputByLabel('Weight').type(weight);
    // Click Save
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Source system saved');
    // Check source system appears in list
    siderList().should('contains.text', name);
    // Check fields
    inputByLabel('Name').should('have.value', name);
    textAreaByLabel('Description').should('have.value', description);
    inputByLabel('Weight').should('have.value', weight);
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Edit source system', () => {
    // Prepare data
    const name = faker.string.alpha({ length: 10, casing: 'upper' });
    const weight = '20';
    const updWeight = '48';
    cy.createDataSource(name, weight);
    goToSourceSystems();
    // Open created source system
    siderList().contains(name).click();
    // Edit source system weight
    inputByLabel('Weight').click().clear().type(updWeight);
    // Click Save
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Source system saved');
    // Check updated field
    inputByLabel('Weight').should('have.value', updWeight);
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

  it('Delete source system', () => {
    // Prepare data
    const name = faker.string.alpha({ length: 10, casing: 'upper' });
    cy.createDataSource(name, faker.number.int({ min: 0, max: 100 }).toString());
    goToSourceSystems();
    // Open created source system
    siderList().contains(name).click();
    // Click Delete
    headerButton(2).click();
    // Confirmation box should appear
    messageDialog().should('be.visible');
    // Click Continue button
    messageDialog().find('button[data-qaid=confirm]').click();
    // Check popup notification message
    check.toast('Source system deleted');
    // Check source system not appears in list
    siderList().should('not.contain.text', name);
    // Check editor is closed
    editor('Source system').should('not.exist');
  });

  it('Source system: Required fields', () => {
    goToSourceSystems();
    // Click Add source system
    actionsButton(1).click();
    // Fill source system fields
    textAreaByLabel('Description').type("something");
    // Click Save
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Validation error');
    // Checking for an error
    check.fieldErrorText('Name', 'Name is required');
  });

  it('Source system: Incorrect values', () => {
    goToSourceSystems();
    // Click Add source system
    actionsButton(1).click();
    // Fill source system fields
    inputByLabel('Name').type('smth!');
    // Click Save
    cy.contains('button', 'Save').click();
    // Check popup notification message
    check.toast('Validation error');
    // Checking for an error
    check.fieldErrorText('Name', 'The value must contain latin letters, numbers, symbols "-", "_" and start with a letter. Cannot contain spaces.');
  });

  it('Source system: Save button state', () => {
    goToSourceSystems();
    // Click Add button
    actionsButton(1).click();
    // Check Save button is disabled
    check.buttonIsDisabled(cy.contains('button', 'Save'));
    // Fill some property
    inputByLabel('Name').type('smth');
    // Check Save button is enabled
    check.buttonIsEnabled(cy.contains('button', 'Save'));
    // Clear the value
    inputByLabel('Name').clear();
    // Check Save button is disabled again
    check.buttonIsDisabled(cy.contains('button', 'Save'));
  });

});

