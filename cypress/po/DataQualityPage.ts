import Page from "./Page";
import PublishDraftDialog from './PublishDraftDialog';

export default class DataQualityPage extends Page {

    publish(msg: string = 'Draft is published') {
        cy.contains('button', 'Publish').click();
        new PublishDraftDialog().clickConfirm();
        this.checkToast(msg);
    }

}