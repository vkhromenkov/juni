import Page from "./Page";
import PublishDraftDialog from './PublishDraftDialog';
import { actionsButton } from "../units/components";

export default class ModelPage extends Page {

    publish(msg: string = 'Draft is published') {
        actionsButton(2).click();
        new PublishDraftDialog().clickConfirm();
        this.checkToast(msg);
    }

}