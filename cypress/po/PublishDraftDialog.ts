export default class PublishDraftDialog {

    private root() {
        return cy.get('div[data-qaid=draft-publish-confirm]>div').should('be.visible');
    }

    clickCancel() {
        this.root().find('button[data-qaid=cancelConfirm]').click();
    }

    clickConfirm() {
        this.root().find('button[data-qaid=confirm]').click();
    }

    checkWarningIsDisplayed() {
        this.root().find('div[class^=noChangesWarning]').should('be.visible');
    }

}