import Page from "./Page";
import PublishDraftDialog from './PublishDraftDialog';

export default class RecordPage extends Page {

    publish(msg: string = 'Draft is published') {
        cy.contains('button', 'Publish').click();
        new PublishDraftDialog().clickConfirm();
        this.checkToast(msg);
    }

    restore(msg: string = 'Record draft is restored') {
        cy.contains('button', 'Restore').click();
        this.checkToast(msg);
    }

}