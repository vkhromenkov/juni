import './rest/core';
import './rest/data';
import './rest/dq';
import './rest/draft';
import './rest/matching';
import './rest/meta';
import './rest/search';
import '@mmisty/cypress-allure-adapter/support';
import '@cypress/xpath';
import 'cypress-network-idle';

declare global {
  namespace Cypress {
    interface Chainable {
      get<S = any>(alias: string, options?: Partial<Loggable & Timeoutable & Withinable & Shadow>): Chainable<S>;
      /**
       * Custom command to login via REST.
       * @example cy.login('admin', 'admin')
       */
      login(username: string, password: string): Chainable<Response<any>>;
      /**
       * Custom command to logout via REST.
       * @example cy.logout()
       */
      logout(): Chainable<Response<any>>;
      /**
       * Custom command to create user via REST.
       * @example cy.createUser('Senior', 'Manager', 'smanager')
       */
      createUser(firstName: string, lastName: string, login: string): Chainable<Response<any>>;
      /**
       * Custom command to create role via REST.
       * @example cy.createRole('dataSteward', 'Data Steward')
       */
      createRole(name: string, displayName: string): Chainable<Response<any>>;
      /**
       * Custom command to create security label via REST.
       * @example cy.createLabel('label', 'Label')
       */
      createLabel(name: string, displayName: string): Chainable<Response<any>>;
      /**
       * Custom command to create record via REST.
       * @example cy.createRecord(requestBody)
       */
      createRecord(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to delete record via REST.
       * @example cy.deleteRecord('person', 'etalon-id')
       */
      deleteRecord(entityName: string, etalonId: string): Chainable<Response<any>>;
      /**
       * Custom command to delete record via REST.
       * @example cy.deleteRecord('person', 'etalon-id')
       */
      deleteRecordTimeline(entityName: string, etalonId: string, validFrom: string, validTo: string): Chainable<Response<any>>;
      /**
       * Custom command to retrieve record info via REST.
       * @example cy.getRecordInfo('etalon-id')
       */
      getRecordInfo(etalonId: string): Chainable<Response<any>>;
      /**
       * Custom command to search for data records via REST.
       * @example cy.search(requestBody)
       */
      search(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to create draft via REST.
       * @example cy.createDraft(requestBody)
       */
      createDraft(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to publish draft via REST.
       * @example cy.publishDraft(21)
       */
      publishDraft(draftId: number): Chainable<Response<any>>;
      /**
       * Custom command to remove all drafts via REST.
       * @example cy.removeAllDrafts()
       */
      removeAllDrafts(): Chainable<Response<any>>;
      /**
       * Custom command to create job via REST.
       * @example cy.createJob(requestBody)
       */
      createJob(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to create data source via REST.
       * @example cy.createDataSource('unidata', '100')
       */
      createDataSource(name: string, weight: string): Chainable<Response<any>>;
      /**
       * Custom command to create enumeration via REST.
       * @example cy.createEnumeration('valueEnum', 'Value Enum', 'value', 'Value')
       */
      createEnumeration(name: string, displayName: string, valueId: string, valueDisplayName: string): Chainable<Response<any>>;
      /**
       * Custom command to create unit via REST.
       * @example cy.createUnit('unit', 'Unit', 'value', 'Value')
       */
      createUnit(name: string, displayName: string, valueId: string, valueDisplayName: string): Chainable<Response<any>>;
      /**
       * Custom command to create group via REST under ROOT.
       * @example cy.createGroups('GroupName')
       */
      createGroup(displayName: string): Chainable<Response<any>>;
      /**
       * Custom command to create entity via REST.
       * @example cy.createEntity(requestBody)
       */
      createEntity(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to create lookup via REST.
       * @example cy.createLookup(requestBody)
       */
      createLookup(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to create nested via REST.
       * @example cy.createNested(requestBody)
       */
      createNested(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to create DQ rule via REST.
       * @example cy.createRule(requestBody)
       */
      createRule(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to create set of DQ rules via REST.
       * @example cy.createSetOfRules(requestBody)
       */
      createSetOfRules(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to create DQ assignments via REST.
       * @example cy.createAssignments('set', 'person')
       */
      createAssignments(setName: string, entityName: string): Chainable<Response<any>>;
      /**
       * Custom command to create pipeline via REST.
       * @example cy.createPipeline(requestBody)
       */
      createPipeline(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to вудуеу pipeline via REST.
       * @example cy.deletePipeline('id')
       */
      deletePipeline(id: string): Chainable<Response<any>>;
      /**
       * Custom command to delete libraries via REST.
       * @example cy.deleteLibraries('lib.jar')
       */
      deleteLibraries(filename: string): Chainable<Response<any>>;
      /**
       * Custom command to delete user notifications via REST.
       * @example cy.deleteNotifications()
       */
      deleteNotifications(): Chainable<Response<any>>;
      /**
       * Custom command to retrieve current user notification count via REST.
       * @example cy.getNotificationCount()
       */
      getNotificationCount(): Chainable<Response<any>>;
      /**
       * Custom command to create matching table via REST.
       * @example cy.createMatchingTable(requestBody)
       */
      createMatchingTable(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to create matching rule via REST.
       * @example cy.createMatchingRule(requestBody)
       */
      createMatchingRule(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to create matching set via REST.
       * @example cy.createMatchingSet(requestBody)
       */
      createMatchingSet(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to create matching assignment via REST.
       * @example cy.createMatchingAssignment(requestBody)
       */
      createMatchingAssignment(requestBody: any): Chainable<Response<any>>;
      /**
       * Custom command to import data model via REST.
       * @example cy.importDataModel('test_model/model.xml', true)
       */
      importDataModel(filePath: string, recreate: boolean): Chainable<Response<any>>;
      /**
       * Custom command to import source systems via REST.
       * @example cy.importSourceSystems('test_model/source-systems.xml')
       */
      importSourceSystems(filePath: string): Chainable<Response<any>>;
      /**
       * Custom command to import enumerations via REST.
       * @example cy.importEnumerations('test_model/enumerations.xml')
       */
      importEnumerations(filePath: string): Chainable<Response<any>>;
      /**
       * Custom command to import units via REST.
       * @example cy.importEnumerations('test_model/measure.xml')
       */
      importUnits(filePath: string): Chainable<Response<any>>;
      /**
       * Custom command to import matching model via REST.
       * @example cy.importMatchingModel('test_model/matching.xml')
       */
      importMatchingModel(filePath: string): Chainable<Response<any>>;
      /**
       * Custom command to import DQ model via REST.
       * @example cy.importMatchingModel('test_model/data-quality.xml')
       */
      importDQModel(filePath: string): Chainable<Response<any>>;
      /**
       * Custom command to set password for given user via REST.
       * @example cy.setPassword('admin', 'admin')
       */
      setPassword(username: string, password: string): Chainable<Response<any>>;
    }
  }
}

Cypress.on('uncaught:exception', () => false);

// Set browser locale to en-US
Cypress.on('window:before:load', window => {
  Object.defineProperty(window.navigator, 'language', { value: 'en-US' });
  Object.defineProperty(window.navigator, 'languages', { value: ['en'] });
  Object.defineProperty(window.navigator, 'accept_languages', { value: ['en'] });
});

before(() => {
  cy.login(Cypress.env('login'), Cypress.env('password'));
  cy.get('@token').then(token => {
    cy.log(token);
    Cypress.env('token', token);
  });
  cy.allure().writeEnvironmentInfo({
    FRAMEWORK: 'Juni (Cypress ' + Cypress.version + ')',
    PLATFORM: Cypress.platform,
    BROWSER_NAME: Cypress.browser.displayName,
    BROWSER_VERSION: Cypress.browser.version
  });
});

beforeEach(() => {
  cy.removeAllDrafts();
  cy.deleteNotifications();
});

after(() => {
  cy.logout();
});
