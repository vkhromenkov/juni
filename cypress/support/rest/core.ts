Cypress.Commands.add('login', (username, password) => {
  Cypress.log({
    name: 'login',
    message: `${username} | ${password}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/core/authentication/login',
    body: {
      'userName': username,
      'password': password,
      'locale': 'en'
    },
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
    cy.wrap(response.body.token).as('token');
  });
});

Cypress.Commands.add('logout', () => {
  Cypress.log({
    name: 'logout'
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/core/authentication/logout',
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.status).to.eq(202);
  });
});

Cypress.Commands.add('setPassword', (username, password) => {
  Cypress.log({
    name: 'setPassword',
    message: `${username} | ${password}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/core/authentication/setpassword',
    body: {
      'userName': username,
      'password': password
    },
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.status).to.eq(200);
  });
});

Cypress.Commands.add('createUser', (firstName, lastName, login) => {
  Cypress.log({
    name: 'createUser',
    message: `${firstName} | ${lastName} | ${login}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/core/security/user',
    body: {
      'user': {
        'active': true,
        'admin': false,
        'email': login + '@unidata.com',
        'emailNotification': false,
        'endpoints': [{ 'name': 'REST' }],
        'external': false,
        'firstName': firstName,
        'lastName': lastName,
        'locale': 'en',
        'login': login,
        'password': login,
        'type': 'USER_DEFINED'
      }
    },
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createRole', (name, displayName) => {
  Cypress.log({
    name: 'createRole',
    message: `${name} | ${displayName}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/core/security/role',
    body: {
      'role': {
        'displayName': displayName,
        'name': name,
        'rights': [],
        'type': 'USER_DEFINED'
      }
    },
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createLabel', (name, displayName) => {
  Cypress.log({
    name: 'createLabel',
    message: `${name} | ${displayName}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/core/security/label',
    body: {
      'securityLabel': {
        'displayName': displayName,
        'name': name,
        'attributes': [{
          'id': 0,
          'path': 'person.citizenship'
        }]
      }
    },
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createJob', (job) => {
  Cypress.log({
    name: 'createJob',
    message: `${job}`
  });
  return cy.request({
    method: 'PUT',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/core/jobs/definitions',
    body: job,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
    cy.wrap(response.body.definition.id).as('jobId');
  });
});

Cypress.Commands.add('createPipeline', (pipeline) => {
  Cypress.log({
    name: 'createPipeline',
    message: `${pipeline}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/core/system/pipeline',
    body: pipeline,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('deletePipeline', (id) => {
  Cypress.log({
    name: 'deletePipeline',
    message: `${id}`
  });
  return cy.request({
    method: 'DELETE',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + `/core/system/pipeline/${id}`,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('deleteLibraries', (fileName) => {
  Cypress.log({
    name: 'deleteLibraries',
    message: `${fileName}`
  });
  return cy.request({
    method: 'DELETE',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + `/core/libraries/${fileName}`,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body).to.have.property('success', true);
  });
});

Cypress.Commands.add('deleteNotifications', () => {
  Cypress.log({
    name: 'deleteNotifications'
  });
  return cy.request({
    method: 'DELETE',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/core/user/notifications',
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body).to.have.property('success', true);
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('getNotificationCount', () => {
  Cypress.log({
    name: 'getNotificationCount'
  });
  return cy.request({
    method: 'GET',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/core/user/notifications/count',
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
    cy.wrap(response.body.count).as('count');
  });
});
