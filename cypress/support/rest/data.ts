import { faker } from '@faker-js/faker';

Cypress.Commands.add('createRecord', (requestBody) => {
  Cypress.log({
    name: 'createRecord',
    message: `${requestBody}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/data/records/upsert',
    body: requestBody,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
    cy.wrap(response.body.recordKeys.etalonKey.id).as('etalonId');
    cy.wrap(response.body.etalon).as('record');
  });
});

Cypress.Commands.add('deleteRecord', (entityName, etalonId) => {
  Cypress.log({
    name: 'deleteRecord',
    message: `${entityName} | ${etalonId}`
  });
  return cy.request({
    method: 'DELETE',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + `/data/records/delete/${entityName}/${etalonId}`,
    headers: {
      'Authorization': Cypress.env('token')
    },
    qs: { inactivateEtalon: true }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('deleteRecordTimeline', (entityName, etalonId, validFrom, validTo) => {
  Cypress.log({
    name: 'deleteRecordTimeline',
    message: `${entityName} | ${etalonId} | ${validFrom} | ${validTo}`
  });
  return cy.request({
    method: 'DELETE',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + `/data/records/delete/${entityName}/${etalonId}`,
    headers: {
      'Authorization': Cypress.env('token')
    },
    qs: { inactivatePeriod: true, validFrom: `${validFrom}`, validTo: `${validTo}` }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('getRecordInfo', (etalonId) => {
  Cypress.log({
    name: 'getRecordInfo',
    message: `${etalonId}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/data/atomic/get',
    body: {
      'payload': {
        [`org.unidata.mdm.rest.${Cypress.env('apiVer')}.data`]: {
          'etalonId': etalonId
        }
      }
    },
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
    cy.wrap(response.body.payload[`org.unidata.mdm.rest.${Cypress.env('apiVer')}.data`].recordKeys).as('keys');
    cy.wrap(response.body.payload[`org.unidata.mdm.rest.${Cypress.env('apiVer')}.matching.data`].results).as('clusters');
  });
});

Cypress.Commands.add('createGroup', (displayName: string) => {
  Cypress.log({
    name: 'createGroup',
    message: `${displayName}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/data/model/entity-groups/' + faker.string.uuid(),
    body: {
      'parentName': 'ROOT',
      'displayName': displayName
    },
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createEntity', (entity) => {
  Cypress.log({
    name: 'createEntity',
    message: `${entity}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/data/model/register-entities/upsert',
    body: entity,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createLookup', (lookup) => {
  Cypress.log({
    name: 'createLookupEntity',
    message: `${lookup}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/data/model/lookup-entities/upsert',
    body: lookup,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createNested', (nested) => {
  Cypress.log({
    name: 'createNested',
    message: `${nested}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/data/model/nested-entities/upsert',
    body: nested,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('importDataModel', (filePath, recreate) => {
  Cypress.log({
    name: 'importDataModel',
    message: `${filePath} | ${recreate}`
  });
  const formData = new FormData();
  cy.fixture(filePath, 'binary').then((file) => {
    formData.set('file', Cypress.Blob.binaryStringToBlob(file, 'text/xml'), 'model.xml');
    formData.set('recreate', recreate.toString());
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/data/model/import',
    body: formData,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.status).to.eq(200);
  });
});

