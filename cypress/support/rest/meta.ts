Cypress.Commands.add('createDataSource', (name, weight) => {
  Cypress.log({
    name: 'createDataSource',
    message: `${name} | ${weight}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/meta/source-systems',
    body: {
      'sourceSystem': {
        'name': name,
        'description': 'Created by Juni framework',
        'weight': weight,
        'customProperties': []
      }
    },
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createEnumeration', (name, displayName, valueId, valueDisplayName) => {
  Cypress.log({
    name: 'createEnumeration',
    message: `${name} | ${displayName} | ${valueId} | ${valueDisplayName}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/meta/enumerations',
    body: {
      'enumeration': {
        'values': [
          {
            'name': valueId + '1',
            'displayName': valueDisplayName + '1'
          },
          {
            'name': valueId + '2',
            'displayName': valueDisplayName + '2'
          }
        ],
        'name': name,
        'displayName': displayName,
        'customProperties': []
      }
    },
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('createUnit', (name, displayName, valueName, valueDisplayName) => {
  Cypress.log({
    name: 'createUnit',
    message: `${name} | ${displayName} | ${valueName}| ${valueDisplayName}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/meta/measurement',
    body: {
      'measurement': {
        'name': name,
        'displayName': displayName,
        'description': 'Created by Juni framework',
        'measurementUnits': [
          {
            'name': valueName + '1',
            'displayName': valueDisplayName + '1',
            'conversionFunction': 'value',
            'reverseConversionFunction': 'value',
            'order': 1,
            'base': true
          },
          {
            'name': valueName + '2',
            'displayName': valueDisplayName + '2',
            'conversionFunction': 'value*2',
            'reverseConversionFunction': 'value/2',
            'order': 2,
            'base': false
          }
        ],
        'customProperties': []
      }
    },
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
  });
});

Cypress.Commands.add('importSourceSystems', (filePath) => {
  Cypress.log({
    name: 'importSourceSystems',
    message: `${filePath}`
  });
  const formData = new FormData();
  cy.fixture(filePath, 'binary').then((file) => {
    formData.set('file', Cypress.Blob.binaryStringToBlob(file, 'text/xml'), 'ss.xml');
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/meta/source-systems/import',
    body: formData,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.status).to.eq(200);
  });
});

Cypress.Commands.add('importEnumerations', (filePath) => {
  Cypress.log({
    name: 'importEnumerations',
    message: `${filePath}`
  });
  const formData = new FormData();
  cy.fixture(filePath, 'binary').then((file) => {
    formData.set('file', Cypress.Blob.binaryStringToBlob(file, 'text/xml'), 'enums.xml');
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/meta/enumerations/import',
    body: formData,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.status).to.eq(200);
  });
});

Cypress.Commands.add('importUnits', (filePath) => {
  Cypress.log({
    name: 'importUnits',
    message: `${filePath}`
  });
  const formData = new FormData();
  cy.fixture(filePath, 'binary').then((file) => {
    formData.set('file', Cypress.Blob.binaryStringToBlob(file, 'text/xml'), 'measure.xml');
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/meta/measurement/import',
    body: formData,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.status).to.eq(200);
  });
});