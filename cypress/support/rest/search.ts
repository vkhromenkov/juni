Cypress.Commands.add('search', (searchRequest) => {
  Cypress.log({
    name: 'search',
    message: `${searchRequest}`
  });
  return cy.request({
    method: 'POST',
    url: Cypress.env('apiUrl') + Cypress.env('apiVer') + '/search',
    body: searchRequest,
    headers: {
      'Authorization': Cypress.env('token')
    }
  }).then((response) => {
    expect(response.body.details.error).to.be.empty;
    cy.wrap(response.body.payload[`org.unidata.mdm.rest.${Cypress.env('apiVer')}.data`].totalCount).as('count');
  });
});
