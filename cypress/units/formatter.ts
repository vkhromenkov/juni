import * as joda from '@js-joda/core';

export function date() {
    return joda.DateTimeFormatter.ofPattern('MM/dd/yyyy');
}

export function dateTime() {
    return joda.DateTimeFormatter.ofPattern('MM/dd/yyyy HH:mm:ss');
}

export function restApi() {
    return joda.DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
}